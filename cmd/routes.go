package main

import (
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/handlers"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func routes(app *config.Application) http.Handler {
	mux := chi.NewRouter()

	// set up middleware
	mux.Use(middleware.Recoverer)
	mux.Use(SessionLoad)
	mux.Use(NoSurf)

	// login routes
	mux.Get("/", handlers.AuthRepo.LoginPage)
	mux.Get("/logout", handlers.AuthRepo.Logout)
	mux.Post("/authenticate", handlers.AuthRepo.CreateAuthToken)
	mux.Post("/is-authenticated", handlers.AuthRepo.IsAuthenticated)

	// system routes
	mux.Route("/sistema", func(mux chi.Router) {
		mux.Use(Auth)

		// home routes
		mux.Get("/home", handlers.PageRepo.HomePage)

		// user routes
		mux.Route("/users", func(mux chi.Router) {
			mux.Use(Admin)

			mux.Get("/", handlers.UserRepo.UsersPage)
			mux.Get("/{id}", handlers.UserRepo.UserEditPage)
			mux.Post("/create-user", handlers.UserRepo.CreateUser)
			mux.Post("/edit-user", handlers.UserRepo.EditUser)
			mux.Put("/edit-one-user/{id}", handlers.UserRepo.EditOneUser)
			mux.Delete("/delete-user/{id}", handlers.UserRepo.DeleteUser)

		})

		// position routes
		mux.Route("/positions", func(mux chi.Router) {
			mux.Use(Admin)

			mux.Get("/", handlers.EmployeeRepo.PositionsPage)
			mux.Post("/create-position", handlers.EmployeeRepo.CreatePosition)
			mux.Put("/update-position", handlers.EmployeeRepo.UpdatePosition)
			mux.Delete("/delete-position/{id}", handlers.EmployeeRepo.DeletePosition)
		})

		// manager routes
		mux.Route("/", func(mux chi.Router) {
			mux.Use(Manager)
			// employee routes
			mux.Get("/employees", handlers.EmployeeRepo.EmployeesPage)
			mux.Post("/employee-create", handlers.EmployeeRepo.CreateEmployee)
			mux.Get("/employee/{id}", handlers.EmployeeRepo.EmployeePage)
			mux.Put("/employee/{id}", handlers.EmployeeRepo.UpdateEmployee)
			mux.Delete("/employee-delete/{id}", handlers.EmployeeRepo.DeleteEmployee)

			// tables
			mux.Get("/tables", handlers.TableRepo.TablesPage)
			mux.Post("/create-table", handlers.TableRepo.CreateTable)
			mux.Put("/update-table/{id}", handlers.TableRepo.UpdateTable)
			mux.Delete("/delete-table/{id}", handlers.TableRepo.DeleteTable)

			// suppliers
			mux.Get("/suppliers", handlers.SupplierRepo.SuppliersPage)
			mux.Post("/create-supplier", handlers.SupplierRepo.CreateSupplier)
			mux.Get("/supplier/{id}", handlers.SupplierRepo.SupplierPage)
			mux.Put("/supplier/{id}", handlers.SupplierRepo.UpdateSupplier)
			mux.Delete("/supplier/{id}", handlers.SupplierRepo.DeleteSupplier)

			// categories
			mux.Get("/categories", handlers.CategoryRepo.CategoriesPage)
			mux.Post("/category", handlers.CategoryRepo.CreateCategory)
			mux.Put("/category/{id}", handlers.CategoryRepo.UpdateCategory)
			mux.Delete("/category/{id}", handlers.CategoryRepo.DeleteCategory)

			// products / purchases
			mux.Get("/products", handlers.ProductRepo.ProductsPage)
			mux.Get("/product/{id}", handlers.ProductRepo.OneProductPage)
			mux.Put("/product/{id}", handlers.ProductRepo.UpdateProduct)
			mux.Delete("/product/{id}", handlers.ProductRepo.DeleteProduct)
			mux.Post("/product", handlers.ProductRepo.CreateProduct)
			mux.Post("/create-purchase", handlers.ProductRepo.CreatePurchase)
			mux.Get("/purchases", handlers.ProductRepo.PurchasesPage)
			mux.Delete("/delete-purchase/{id}", handlers.ProductRepo.DeletePurchase)
			mux.Get("/low-stock", handlers.ProductRepo.LowStockPage)
			mux.Get("/reports-products", handlers.ProductRepo.ReportsProductsPage)
			mux.Get("/reports-purchases", handlers.ProductRepo.ReportsPurchasesPage)

			// financial transactions
			mux.Get("/billpay", handlers.FinancialRepo.BillPayPage)
			mux.Post("/billpay", handlers.FinancialRepo.CreateBillPay)
			mux.Delete("/billpay/{id}", handlers.FinancialRepo.DeleteBillPay)
			mux.Get("/billpay/{id}", handlers.FinancialRepo.BillPayOnePage)
			mux.Put("/billpay/{id}", handlers.FinancialRepo.UpdateBillPay)
			mux.Post("/billpay/write-off/{id}", handlers.FinancialRepo.WriteOffBillPay)
			mux.Get("/bill-receive", handlers.FinancialRepo.BillReceivePage)
			mux.Post("/bill-receive", handlers.FinancialRepo.CreateBillReceive)
			mux.Delete("/bill-receive/{id}", handlers.FinancialRepo.DeleteBillReceive)
			mux.Get("/bill-receive/{id}", handlers.FinancialRepo.BillReceiveOnePage)
			mux.Put("/bill-receive/{id}", handlers.FinancialRepo.UpdateBillReceive)
			mux.Post("/bill-receive/write-off/{id}", handlers.FinancialRepo.WriteOffBillReceive)
			mux.Get("/moviments", handlers.FinancialRepo.PurchaseMovimentPage)
			mux.Get("/reports-billpays", handlers.FinancialRepo.ReportsBillPaysPage)
			mux.Get("/reports-billreceives", handlers.FinancialRepo.ReportsBillReceivesPage)
			mux.Get("/reports-moviments", handlers.FinancialRepo.ReportsMovimentsPage)

			// dishes
			mux.Get("/dishes", handlers.DishRepo.DishesPage)
			mux.Post("/create-dish", handlers.DishRepo.CreateDish)
			mux.Put("/update-dish/{id}", handlers.DishRepo.UpdateDish)
			mux.Put("/update-dish-status/{id}", handlers.DishRepo.UpdateDishStatus)
			mux.Delete("/delete-dish/{id}", handlers.DishRepo.DeleteDish)
			mux.Get("/reports-dishes", handlers.DishRepo.ReportsDishesPage)
		})

		// reception / reservations routes
		mux.Get("/reservations", handlers.ReceptionRepo.ReservationsPage)
		mux.Get("/all-reservations", handlers.ReceptionRepo.AllReservations)
		mux.Get("/reservations-tables", handlers.ReceptionRepo.ReservationsTables)
		mux.Post("/make-reservation", handlers.ReceptionRepo.MakeReservation)
		mux.Delete("/delete-reservation", handlers.ReceptionRepo.DeleteReservation)
		mux.Delete("/reservation/{id}", handlers.ReceptionRepo.DeleteReservationByID)
		mux.Get("/count-reservations", handlers.ReceptionRepo.CountReservationsByDate)

		// client routes
		mux.Get("/clients", handlers.ClientRepo.ClientsPage)
		mux.Post("/clients", handlers.ClientRepo.CreateClient)
		mux.Put("/clients/{id}", handlers.ClientRepo.UpdateClient)
		mux.Delete("/clients/{id}", handlers.ClientRepo.DeleteClient)

	})

	// 404 page not found
	mux.NotFound(handlers.PageRepo.NotFoundPage)

	// set up static file server
	fileServe := http.FileServer(http.Dir("./static/"))
	mux.Handle("/static/*", http.StripPrefix("/static", fileServe))

	return mux
}
