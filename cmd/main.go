package main

import (
	"context"
	"encoding/gob"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/handlers"
	"restaurante/internal/helpers"
	"restaurante/internal/render"
	"time"

	"github.com/alexedwards/scs/pgxstore"
	"github.com/alexedwards/scs/v2"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/joho/godotenv"
)

const portNumber = ":8080"

var app config.Application
var session *scs.SessionManager
var infoLog *log.Logger
var errorLog *log.Logger

func main() {
	var err error

	if err = godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	db, pool, err := run()
	if err != nil {
		log.Fatal(err)
	}
	defer db.SQL.Close()
	defer pool.Close()

	// start server
	fmt.Printf("Starting application on port %s\n", portNumber)
	srv := &http.Server{
		Addr:    portNumber,
		Handler: routes(&app),
	}

	err = srv.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

func run() (*driver.DB, *pgxpool.Pool, error) {
	// what am I going to put in the session
	gob.Register(map[string]int{})

	// read flags
	inProduction := flag.Bool("production", true, "Application is in production")
	useCache := flag.Bool("cache", true, "Use template cache")

	flag.Parse()

	// set up cache when in production
	app.InProduction = *inProduction
	app.UseCache = *useCache

	// set up logger
	infoLog = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	app.InfoLog = infoLog

	errorLog = log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	app.ErrorLog = errorLog

	// configure database
	connectString := fmt.Sprintf("host=%s port=%s dbname=%s user=%s password=%s sslmode=disable", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"))

	db, err := driver.ConnectSQL(connectString)
	if err != nil {
		log.Fatal("cannot connect to database! Dying...")
		return nil, nil, err
	}
	log.Println("Connected to database")

	// configure pgxpool
	pool, err := pgxpool.New(context.Background(), connectString)
	if err != nil {
		log.Fatal("cannot connect to database! Dying...")
	}

	// set up session
	session = scs.New()
	session.Lifetime = 24 * time.Hour
	session.Store = pgxstore.New(pool)
	session.Cookie.Persist = true
	session.Cookie.SameSite = http.SameSiteLaxMode
	session.Cookie.Secure = app.InProduction

	app.Session = session

	// configure template cache
	tc, err := render.CreateTemplateCache()
	if err != nil {
		log.Fatal("cannot create template cache", err)
		return nil, nil, err
	}

	app.TemplateCache = tc

	// configure handlers
	repo := handlers.NewPageRepo(&app, db)
	handlers.NewPageHandlers(repo)
	handlers.NewAuthHandlers(handlers.NewAuthRepo(&app, db))
	handlers.NewUserHandlers(handlers.NewUserRepo(&app, db))
	handlers.NewEmployeeHandlers(handlers.NewEmployeeRepo(&app, db))
	handlers.NewTableHandlers(handlers.NewTableRepo(&app, db))
	handlers.NewSupplierHandlers(handlers.NewSupplierRepo(&app, db))
	handlers.NewCategoryHandlers(handlers.NewCategoryRepo(&app, db))
	handlers.NewProductHandlers(handlers.NewProductRepo(&app, db))
	handlers.NewDishHandlers(handlers.NewDishRepo(&app, db))
	handlers.NewReceptionHandlers(handlers.NewReceptionRepo(&app, db))
	handlers.NewClientHandlers(handlers.NewClientRepo(&app, db))
	handlers.NewFinancialHandlers(handlers.NewFinancialRepo(&app, db))
	render.NewRenderer(&app, db.SQL)
	helpers.NewHelpers(&app)

	return db, pool, nil
}
