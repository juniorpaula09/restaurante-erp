package main

import (
	"net/http"
	"restaurante/internal/helpers"

	"github.com/justinas/nosurf"
)

// SessionLoad loads and saves the session on every request
func SessionLoad(next http.Handler) http.Handler {
	return session.LoadAndSave(next)
}

// NoSurf adds CSRF protection to all POST requests
func NoSurf(next http.Handler) http.Handler {
	csrfHandler := nosurf.New(next)

	// set base cookie
	csrfHandler.SetBaseCookie(http.Cookie{
		HttpOnly: true,
		Path:     "/",
		Secure:   app.InProduction,
		SameSite: http.SameSiteLaxMode,
	})

	return csrfHandler
}

// Auth checks if user is authenticated
func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// if user is not authenticated
		if !helpers.IsAuthenticated(r) {
			// redirect to login page
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}

		// if user is authenticated
		next.ServeHTTP(w, r)
	})
}

// Admin checks if user is an admin
func Admin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// if user is not an admin
		if !helpers.IsAdmin(r) {
			// redirect to home page
			http.Redirect(w, r, "/sistema/reservations", http.StatusSeeOther)
			return
		}

		// if user is an admin
		next.ServeHTTP(w, r)
	})
}

// Manager checks if user is a manager
func Manager(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// if user is not a manager
		if !helpers.IsManager(r) {
			// redirect to home page
			http.Redirect(w, r, "/sistema/reservations", http.StatusSeeOther)
			return
		}

		// if user is a manager
		next.ServeHTTP(w, r)
	})
}
