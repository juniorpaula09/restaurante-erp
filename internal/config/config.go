package config

import (
	"html/template"
	"log"

	"github.com/alexedwards/scs/v2"
)

// Application holds the application config
type Application struct {
	UseCache      bool
	TemplateCache map[string]*template.Template
	InfoLog       *log.Logger
	ErrorLog      *log.Logger
	InProduction  bool
	Session       *scs.SessionManager
}
