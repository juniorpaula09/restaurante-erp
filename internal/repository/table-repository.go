package repository

import "restaurante/internal/models"

type TableRepository interface {
	AllTables() ([]*models.Table, error)
	CreateTable(table *models.Table) error
	UpdateTable(id int, table *models.Table) error
	DeleteTable(id int) error
	GetTableByID(id int) (*models.Table, error)
}
