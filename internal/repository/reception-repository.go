package repository

import "restaurante/internal/models"

type ReceptionRepository interface {
	GetAllReservations() ([]*models.Reservation, error)
	CreateReservation(r *models.Reservation) error
	GetReservationsByTableIDAndDate(tableID int, date string) ([]*models.Reservation, error)
	DeleteReservationByTableID(tableID int) error
	DeleteReservationByID(id int) error
	CountReservationByDate(start string, end string) (int, error)
}
