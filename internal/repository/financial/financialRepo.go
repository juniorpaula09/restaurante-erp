package financial

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

// postgresDBRepo represents the postgres repository
type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

// NewPostgresRepo creates a new postgres repository
func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.FinancialRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAllBillPays gets all bill pays
func (m *postgresDBRepo) GetAllBillPays() ([]models.BillPayable, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			bp.id, bp.amount, bp.description, bp.user_id, bp.purchase_moviment_id, bp.status, bp.due_date, bp.bill_date, bp.created_at, bp.updated_at,
			u.id, u.first_name, u.email	
		FROM bill_payables bp
		INNER JOIN users u ON (u.id = bp.user_id)
		ORDER BY bp.id DESC
	`

	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var billpays []models.BillPayable

	for rows.Next() {
		bp := models.BillPayable{}
		u := models.User{}

		err := rows.Scan(
			&bp.ID,
			&bp.Amount,
			&bp.Description,
			&bp.UserID,
			&bp.PurchaseMovimentID,
			&bp.Status,
			&bp.DueDate,
			&bp.BillDate,
			&bp.CreatedAt,
			&bp.UpdatedAt,
			&u.ID,
			&u.FirstName,
			&u.Email,
		)
		if err != nil {
			return nil, err
		}

		bp.User = u

		billpays = append(billpays, bp)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return billpays, nil
}

// GetBillPayByID gets a bill pay by id
func (m *postgresDBRepo) GetBillPayByID(id int) (models.BillPayable, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT 
			bp.id, bp.amount, bp.description, bp.user_id, bp.purchase_moviment_id, bp.status, bp.due_date, bp.bill_date, bp.created_at, bp.updated_at,
			u.id, u.first_name, u.email
		FROM bill_payables bp
		INNER JOIN users u ON (u.id = bp.user_id)
		WHERE bp.id = $1
	`

	row := m.DB.QueryRowContext(ctx, query, id)

	bp := models.BillPayable{}

	err := row.Scan(
		&bp.ID,
		&bp.Amount,
		&bp.Description,
		&bp.UserID,
		&bp.PurchaseMovimentID,
		&bp.Status,
		&bp.DueDate,
		&bp.BillDate,
		&bp.CreatedAt,
		&bp.UpdatedAt,
		&bp.User.ID,
		&bp.User.FirstName,
		&bp.User.Email,
	)
	if err != nil {
		return bp, err
	}

	return bp, nil
}

// DeleteBillPay deletes a bill pay
func (m *postgresDBRepo) DeleteBillPay(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		DELETE FROM bill_payables
		WHERE id = $1
	`

	_, err := m.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}

	return nil
}

// DeletePurchaseMovimentByID deletes a purchase moviment by id
func (m *postgresDBRepo) DeletePurchaseMovimentByID(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		DELETE FROM purchase_moviments
		WHERE id = $1
	`

	_, err := m.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}

	return nil
}

// UpdateBillPay updates a bill pay
func (m *postgresDBRepo) UpdateBillPay(billpay models.BillPayable) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE bill_payables
		SET amount = $1, description = $2, user_id = $3, purchase_moviment_id = $4, status = $5, due_date = $6, bill_date = $7, updated_at = $8
		WHERE id = $9
	`

	_, err := m.DB.ExecContext(ctx, query,
		billpay.Amount,
		billpay.Description,
		billpay.UserID,
		billpay.PurchaseMovimentID,
		billpay.Status,
		billpay.DueDate,
		billpay.BillDate,
		time.Now(),
		billpay.ID,
	)
	if err != nil {
		return err
	}

	query = `
		UPDATE purchase_moviments
		SET amount = $1, updated_at = $2
		WHERE id = $3
	`
	_, err = m.DB.ExecContext(ctx, query,
		billpay.Amount,
		time.Now(),
		billpay.PurchaseMovimentID,
	)
	if err != nil {
		return err
	}

	return nil
}

// GetPurchaseMovimentByID gets a purchase moviment by id
func (m *postgresDBRepo) GetPurchaseMovimentByID(id int) (models.PurchaseMoviment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			pm.id, pm.amount, pm.purchase_date, pm.user_id, pm.supplier_id, pm.status, pm.created_at, pm.updated_at, pm.type,
			u.id, u.first_name, u.email
		FROM purchase_moviments pm
		INNER JOIN users u ON (u.id = pm.user_id)
		WHERE pm.id = $1
	`

	row := m.DB.QueryRowContext(ctx, query, id)

	pm := models.PurchaseMoviment{}

	err := row.Scan(
		&pm.ID,
		&pm.Amount,
		&pm.PurchaseDate,
		&pm.UserID,
		&pm.SupplierID,
		&pm.Status,
		&pm.CreatedAt,
		&pm.UpdatedAt,
		&pm.Type,
		&pm.User.ID,
		&pm.User.FirstName,
		&pm.User.Email,
	)

	if err != nil {
		return pm, err
	}

	return pm, nil
}

// UpdatePuchaseMoviment updates a purchase moviment
func (m *postgresDBRepo) UpdatePuchaseMoviment(purchase models.PurchaseMoviment) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE purchase_moviments
		SET amount = $1, purchase_date = $2, user_id = $3, supplier_id = $4, status = $5, updated_at = $6, type = $7
		WHERE id = $8
	`

	_, err := m.DB.ExecContext(ctx, query,
		purchase.Amount,
		purchase.PurchaseDate,
		purchase.UserID,
		purchase.SupplierID,
		purchase.Status,
		time.Now(),
		purchase.Type,
		purchase.ID,
	)
	if err != nil {
		return err
	}

	return nil
}

// GetAllBillReceives gets all bill receives
func (m *postgresDBRepo) GetAllBillReceives() ([]models.BillReceive, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			br.id, br.amount, br.description, br.user_id, br.purchase_moviment_id, br.status, br.due_date, br.bill_date, br.created_at, br.updated_at,
			u.id, u.first_name, u.email
		FROM bill_receives br
		INNER JOIN users u ON (u.id = br.user_id)
		ORDER BY br.id DESC
	`

	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var billreceives []models.BillReceive

	for rows.Next() {
		br := models.BillReceive{}
		u := models.User{}

		err := rows.Scan(
			&br.ID,
			&br.Amount,
			&br.Description,
			&br.UserID,
			&br.PurchaseMovimentID,
			&br.Status,
			&br.DueDate,
			&br.BillDate,
			&br.CreatedAt,
			&br.UpdatedAt,
			&u.ID,
			&u.FirstName,
			&u.Email,
		)
		if err != nil {
			return nil, err
		}

		br.User = u

		billreceives = append(billreceives, br)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return billreceives, nil
}

// CreateBillReceive creates a bill receive
func (m *postgresDBRepo) CreateBillReceive(billreceive models.BillReceive) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		INSERT INTO bill_receives
		(amount, description, user_id, purchase_moviment_id, status, due_date, bill_date, created_at, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`

	_, err := m.DB.ExecContext(ctx, query,
		billreceive.Amount,
		billreceive.Description,
		billreceive.UserID,
		billreceive.PurchaseMovimentID,
		billreceive.Status,
		billreceive.DueDate,
		billreceive.BillDate,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		return err
	}

	return nil
}

// GetBillReceiveByID gets a bill receive by id
func (m *postgresDBRepo) GetBillReceiveByID(id int) (models.BillReceive, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			br.id, br.amount, br.description, br.user_id, br.purchase_moviment_id, br.status, br.due_date, br.bill_date, br.created_at, br.updated_at,
			u.id, u.first_name, u.email
		FROM bill_receives br
		INNER JOIN users u ON (u.id = br.user_id)
		WHERE br.id = $1
	`

	row := m.DB.QueryRowContext(ctx, query, id)

	br := models.BillReceive{}

	err := row.Scan(
		&br.ID,
		&br.Amount,
		&br.Description,
		&br.UserID,
		&br.PurchaseMovimentID,
		&br.Status,
		&br.DueDate,
		&br.BillDate,
		&br.CreatedAt,
		&br.UpdatedAt,
		&br.User.ID,
		&br.User.FirstName,
		&br.User.Email,
	)
	if err != nil {
		return br, err
	}

	return br, nil
}

// DeleteBillReceive deletes a bill receive
func (m *postgresDBRepo) DeleteBillReceive(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		DELETE FROM bill_receives
		WHERE id = $1
	`

	_, err := m.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}

	return nil
}

// UpdateBillReceive updates a bill receive
func (m *postgresDBRepo) UpdateBillReceive(billreceive models.BillReceive) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE bill_receives
		SET amount = $1, description = $2, user_id = $3, purchase_moviment_id = $4, status = $5, due_date = $6, bill_date = $7, updated_at = $8
		WHERE id = $9
	`

	_, err := m.DB.ExecContext(ctx, query,
		billreceive.Amount,
		billreceive.Description,
		billreceive.UserID,
		billreceive.PurchaseMovimentID,
		billreceive.Status,
		billreceive.DueDate,
		billreceive.BillDate,
		time.Now(),
		billreceive.ID,
	)
	if err != nil {
		return err
	}

	query = `
		UPDATE purchase_moviments
		SET amount = $1, updated_at = $2
		WHERE id = $3
	`
	_, err = m.DB.ExecContext(ctx, query,
		billreceive.Amount,
		time.Now(),
		billreceive.PurchaseMovimentID,
	)
	if err != nil {
		return err
	}

	return nil
}

// GetAllPurchaseMoviments gets all purchase moviments
func (m *postgresDBRepo) GetAllPurchaseMoviments() ([]models.PurchaseMoviment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			pm.id, pm.amount, pm.purchase_date, pm.user_id, pm.supplier_id, pm.status, pm.created_at, pm.updated_at, pm.type,
			u.id, u.first_name, u.email
		FROM purchase_moviments pm
		INNER JOIN users u ON (u.id = pm.user_id)
		ORDER BY pm.id DESC
	`

	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var purchases []models.PurchaseMoviment

	for rows.Next() {
		pm := models.PurchaseMoviment{}
		u := models.User{}

		err := rows.Scan(
			&pm.ID,
			&pm.Amount,
			&pm.PurchaseDate,
			&pm.UserID,
			&pm.SupplierID,
			&pm.Status,
			&pm.CreatedAt,
			&pm.UpdatedAt,
			&pm.Type,
			&u.ID,
			&u.FirstName,
			&u.Email,
		)
		if err != nil {
			return nil, err
		}

		pm.User = u

		purchases = append(purchases, pm)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return purchases, nil
}

// GetAllBillpaysFromReport gets all bill pays from report
func (m *postgresDBRepo) GetAllBillpaysFromReport(startDate, endDate, status string) ([]models.BillPayable, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			bp.id, bp.amount, bp.description, bp.user_id, bp.purchase_moviment_id, bp.due_date, bp.bill_date, bp.status, bp.created_at, bp.updated_at,
			u.id, u.first_name, u.email
		FROM bill_payables bp
		INNER JOIN users u ON (u.id = bp.user_id)
		WHERE bp.bill_date >= $1 AND bp.bill_date <= $2 AND (bp.status LIKE $3 OR $3 = '')
		ORDER BY bp.id ASC
	`

	rows, err := m.DB.QueryContext(ctx, query, startDate, endDate, status)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var billpays []models.BillPayable

	for rows.Next() {
		bp := models.BillPayable{}
		u := models.User{}

		err := rows.Scan(
			&bp.ID,
			&bp.Amount,
			&bp.Description,
			&bp.UserID,
			&bp.PurchaseMovimentID,
			&bp.DueDate,
			&bp.BillDate,
			&bp.Status,
			&bp.CreatedAt,
			&bp.UpdatedAt,
			&u.ID,
			&u.FirstName,
			&u.Email,
		)
		if err != nil {
			return nil, err
		}

		bp.User = u

		billpays = append(billpays, bp)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return billpays, nil
}

// GetAllBillReceiveFromReport gets all bill receives from report
func (m *postgresDBRepo) GetAllBillReceiveFromReport(startDate, endDate, status string) ([]models.BillReceive, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			br.id, br.amount, br.description, br.user_id, br.purchase_moviment_id, br.due_date, br.bill_date, br.status, br.created_at, br.updated_at,
			u.id, u.first_name, u.email
		FROM bill_receives br
		INNER JOIN users u ON (u.id = br.user_id)
		WHERE br.bill_date >= $1 AND br.bill_date <= $2 AND (br.status LIKE $3 OR $3 = '')
		ORDER BY br.id ASC
	`

	rows, err := m.DB.QueryContext(ctx, query, startDate, endDate, status)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var billreceives []models.BillReceive

	for rows.Next() {
		br := models.BillReceive{}
		u := models.User{}

		err := rows.Scan(
			&br.ID,
			&br.Amount,
			&br.Description,
			&br.UserID,
			&br.PurchaseMovimentID,
			&br.DueDate,
			&br.BillDate,
			&br.Status,
			&br.CreatedAt,
			&br.UpdatedAt,
			&u.ID,
			&u.FirstName,
			&u.Email,
		)
		if err != nil {
			return nil, err
		}

		br.User = u

		billreceives = append(billreceives, br)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return billreceives, nil
}

// GetAllMovimentsFromReport gets all moviments from report
func (m *postgresDBRepo) GetAllMovimentsFromReport(startDate, endDate, mType string) ([]models.PurchaseMoviment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT
			pm.id, pm.amount, pm.purchase_date, pm.user_id, pm.supplier_id, pm.status, pm.created_at, pm.updated_at, pm.type,
			u.id, u.first_name, u.email
		FROM purchase_moviments pm
		INNER JOIN users u ON (u.id = pm.user_id)
		WHERE pm.purchase_date >= $1 AND pm.purchase_date <= $2 AND (pm.type LIKE $3 OR $3 = '')
		ORDER BY pm.id ASC
	`

	rows, err := m.DB.QueryContext(ctx, query, startDate, endDate, mType)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var purchases []models.PurchaseMoviment

	for rows.Next() {
		pm := models.PurchaseMoviment{}
		u := models.User{}

		err := rows.Scan(
			&pm.ID,
			&pm.Amount,
			&pm.PurchaseDate,
			&pm.UserID,
			&pm.SupplierID,
			&pm.Status,
			&pm.CreatedAt,
			&pm.UpdatedAt,
			&pm.Type,
			&u.ID,
			&u.FirstName,
			&u.Email,
		)
		if err != nil {
			return nil, err
		}

		pm.User = u

		purchases = append(purchases, pm)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return purchases, nil
}

// GetAllMovimentsByDate gets all moviments by date
func (m *postgresDBRepo) GetAllMovimentsByDate(date string) ([]models.PurchaseMoviment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT * FROM purchase_moviments
		WHERE purchase_date = $1
		ORDER BY id ASC
	`

	rows, err := m.DB.QueryContext(ctx, query, date)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var purchases []models.PurchaseMoviment

	for rows.Next() {
		pm := models.PurchaseMoviment{}

		err := rows.Scan(
			&pm.ID,
			&pm.Amount,
			&pm.PurchaseDate,
			&pm.UserID,
			&pm.SupplierID,
			&pm.Status,
			&pm.CreatedAt,
			&pm.UpdatedAt,
			&pm.Type,
		)
		if err != nil {
			return nil, err
		}

		purchases = append(purchases, pm)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return purchases, nil
}
