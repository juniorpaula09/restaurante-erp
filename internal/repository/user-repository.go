package repository

import "restaurante/internal/models"

type UserRepository interface {
	GetUserByEmail(email string) (models.User, error)
	GetUserByToken(token string) (models.User, error)
	GetUserByID(id int) (models.User, error)
	GetAllUsers() ([]models.User, error)
	UpdateUser(id int, user models.User) error
	CreateUser(user models.User) error
	UpdateOneUser(id int, user models.User) error
	DeleteUser(id int) error
}
