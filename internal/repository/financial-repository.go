package repository

import "restaurante/internal/models"

type FinancialRepository interface {
	GetAllBillPays() ([]models.BillPayable, error)
	GetBillPayByID(id int) (models.BillPayable, error)
	DeleteBillPay(id int) error
	DeletePurchaseMovimentByID(id int) error
	UpdateBillPay(billpay models.BillPayable) error
	GetPurchaseMovimentByID(id int) (models.PurchaseMoviment, error)
	UpdatePuchaseMoviment(purchase models.PurchaseMoviment) error
	GetAllBillReceives() ([]models.BillReceive, error)
	CreateBillReceive(billreceive models.BillReceive) error
	GetBillReceiveByID(id int) (models.BillReceive, error)
	DeleteBillReceive(id int) error
	UpdateBillReceive(billreceive models.BillReceive) error
	GetAllPurchaseMoviments() ([]models.PurchaseMoviment, error)
	GetAllBillpaysFromReport(startDate, endDate, status string) ([]models.BillPayable, error)
	GetAllBillReceiveFromReport(startDate, endDate, status string) ([]models.BillReceive, error)
	GetAllMovimentsFromReport(startDate, endDate, mType string) ([]models.PurchaseMoviment, error)
	GetAllMovimentsByDate(date string) ([]models.PurchaseMoviment, error)
}
