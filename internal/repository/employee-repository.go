package repository

import "restaurante/internal/models"

type EmployeeRepository interface {
	GetAllEmployees() ([]*models.Employee, error)
	CreateEmployee(employee *models.Employee) error
	GetEmployeeByID(id int) (*models.Employee, error)
	UpdateEmployee(employee *models.Employee) error
	DeleteEmployee(id int) error
	GetPositions() ([]*models.Position, error)
	CreatePosition(position *models.Position) error
	UpdatePosition(position *models.Position) error
	DeletePosition(id int) error
}
