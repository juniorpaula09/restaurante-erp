package tables

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.TableRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// AllTables returns all tables
func (m *postgresDBRepo) AllTables() ([]*models.Table, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT id, name, description, created_at, updated_at
		FROM tables
		ORDER BY id ASC
	`

	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var tables []*models.Table

	for rows.Next() {
		t := &models.Table{}
		err := rows.Scan(
			&t.ID,
			&t.Name,
			&t.Description,
			&t.CreatedAt,
			&t.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		tables = append(tables, t)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return tables, nil
}

// CreateTable creates a new table
func (m *postgresDBRepo) CreateTable(table *models.Table) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		INSERT INTO tables (name, description, created_at, updated_at)
		VALUES ($1, $2, $3, $4)
	`

	_, err := m.DB.ExecContext(ctx, query,
		table.Name,
		table.Description,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		return err
	}

	return nil
}

// UpdateTable updates a table
func (m *postgresDBRepo) UpdateTable(id int, table *models.Table) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE tables
		SET name = $1, description = $2, updated_at = $3
		WHERE id = $4
	`

	_, err := m.DB.ExecContext(ctx, query,
		table.Name,
		table.Description,
		time.Now(),
		id,
	)
	if err != nil {
		return err
	}

	return nil
}

// DeleteTable deletes a table
func (m *postgresDBRepo) DeleteTable(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		DELETE FROM tables
		WHERE id = $1
	`

	_, err := m.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}

	return nil
}

// GetTableByID gets a table by id
func (m *postgresDBRepo) GetTableByID(id int) (*models.Table, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT id, name, description, created_at, updated_at
		FROM tables
		WHERE id = $1
	`

	row := m.DB.QueryRowContext(ctx, query, id)

	t := &models.Table{}

	err := row.Scan(
		&t.ID,
		&t.Name,
		&t.Description,
		&t.CreatedAt,
		&t.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return t, nil
}
