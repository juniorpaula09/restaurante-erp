package repository

import "restaurante/internal/models"

type CategoryRepository interface {
	GetAllCategories() ([]*models.Category, error)
	CreateCategory(c *models.Category) error
	UpdateCategory(c *models.Category) error
	DeleteCategory(id int) error
}
