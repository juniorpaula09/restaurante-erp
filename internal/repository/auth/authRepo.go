package auth

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"database/sql"
	"encoding/base32"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

// NewPostgresRepo creates a new repository
func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.AuthRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GenerateToken generates a new token
func (m *postgresDBRepo) GenerateToken(userID int, ttl time.Duration, scope string) (*models.Token, error) {
	token := &models.Token{
		UserID: int64(userID),
		Expiry: time.Now().Add(ttl),
		Scope:  scope,
	}

	randomBytes := make([]byte, 16)
	_, err := rand.Read(randomBytes)
	if err != nil {
		return nil, err
	}

	token.PlainText = base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString(randomBytes)
	hash := sha256.Sum256([]byte(token.PlainText))
	if err != nil {
		return nil, err
	}
	token.Hash = hash[:]

	return token, nil
}

// InsertToken inserts a new token into the database
func (m *postgresDBRepo) InsertToken(t *models.Token, u models.User) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	// delete token if it exists for this user and scope
	stmt := `DELETE FROM tokens WHERE user_id = $1`
	_, err := m.DB.ExecContext(ctx, stmt, t.UserID)
	if err != nil {
		return err
	}

	// insert token into database
	stmt = `
		INSERT INTO 
			tokens (user_id, name, email, token_hash, expiry, created_at, updated_at) 
		VALUES 
			($1, $2, $3, $4, $5, $6, $7)
	`

	_, err = m.DB.ExecContext(ctx, stmt,
		t.UserID,
		u.FirstName,
		u.Email,
		t.Hash,
		t.Expiry,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		return err
	}

	return nil
}
