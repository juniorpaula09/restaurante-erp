package users

import (
	"context"
	"crypto/sha256"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"strings"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

// NewPostgresRepo creates a new repository
func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.UserRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAllUsers gets all users
func (m *postgresDBRepo) GetAllUsers() ([]models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var users []models.User

	rows, err := m.DB.QueryContext(ctx, `
		SELECT id, first_name, last_name, email, access_level
		FROM users
		ORDER BY id ASC
	`)
	if err != nil {
		return users, err
	}
	defer rows.Close()

	for rows.Next() {
		var u models.User

		err := rows.Scan(
			&u.ID,
			&u.FirstName,
			&u.LastName,
			&u.Email,
			&u.AccessLevel,
		)
		if err != nil {
			return users, err
		}

		users = append(users, u)
	}

	if err = rows.Err(); err != nil {
		return users, err
	}

	return users, nil
}

// GetUserByEmail gets a user by email
func (m *postgresDBRepo) GetUserByEmail(email string) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	email = strings.ToLower(email)
	var u models.User

	row := m.DB.QueryRowContext(ctx, `
		SELECT id, first_name, last_name, email, password, access_level, created_at, updated_at
		FROM users
		WHERE email = $1
	`, email)

	err := row.Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.Email,
		&u.Password,
		&u.AccessLevel,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err != nil {
		return u, err
	}

	return u, nil
}

// GetUserByToken gets a user by token
func (m *postgresDBRepo) GetUserByToken(token string) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	tokenHash := sha256.Sum256([]byte(token))
	var u models.User

	row := m.DB.QueryRowContext(ctx, `
		SELECT u.id, u.first_name, u.last_name, u.email, u.password, u.access_level, u.created_at, u.updated_at
		FROM users u
		INNER JOIN tokens t
		ON u.id = t.user_id
		WHERE t.token_hash = $1
	`, tokenHash[:])

	err := row.Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.Email,
		&u.Password,
		&u.AccessLevel,
		&u.CreatedAt,
		&u.UpdatedAt,
	)

	if err != nil {
		return u, err
	}

	return u, nil
}

// GetUserByID gets a user by id
func (m *postgresDBRepo) GetUserByID(id int) (models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var u models.User

	row := m.DB.QueryRowContext(ctx, `
		SELECT id, first_name, last_name, email, password, access_level
		FROM users
		WHERE id = $1
	`, id)

	err := row.Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.Email,
		&u.Password,
		&u.AccessLevel,
	)

	if err != nil {
		return u, err
	}

	return u, nil
}

// UpdateUser update a user by your self
func (m *postgresDBRepo) UpdateUser(id int, user models.User) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := m.DB.ExecContext(ctx, `
		UPDATE users
		SET first_name = $1, last_name = $2, email = $3, updated_at = $4
		WHERE id = $5
	`, user.FirstName, user.LastName, user.Email, time.Now(), id,
	)

	if err != nil {
		return err
	}

	return nil
}

// CreateUser creates a new user
func (m *postgresDBRepo) CreateUser(user models.User) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := m.DB.ExecContext(ctx, `
		INSERT INTO users (first_name, last_name, email, password, access_level, created_at, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`, user.FirstName, user.LastName, user.Email, user.Password, user.AccessLevel, time.Now(), time.Now(),
	)

	if err != nil {
		return err
	}

	return nil
}

// UpdateOneUser updates a user
func (m *postgresDBRepo) UpdateOneUser(id int, user models.User) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	_, err := m.DB.ExecContext(ctx, `
		UPDATE users
		SET first_name = $1, last_name = $2, email = $3, password = $4, access_level = $5, updated_at = $6
		WHERE id = $7
	`, user.FirstName, user.LastName, user.Email, user.Password, user.AccessLevel, time.Now(), id,
	)

	if err != nil {
		return err
	}

	return nil
}

// DeleteUser deletes a user
func (m *postgresDBRepo) DeleteUser(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `DELETE FROM users WHERE id = $1`

	_, err := m.DB.ExecContext(ctx, query, id)

	if err != nil {
		return err
	}

	return nil
}
