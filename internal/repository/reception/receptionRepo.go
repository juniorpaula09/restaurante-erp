package reception

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

// NewPostgresRepo creates a new repository
func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.ReceptionRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAllReservations gets all reservations
func (m *postgresDBRepo) GetAllReservations() ([]*models.Reservation, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `SELECT id, client_id, table_id, employee_id, qtd_people, obs, date, created_at, updated_at
	FROM reservations ORDER BY date ASC`

	rows, err := m.DB.QueryContext(ctx, stmt)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return nil, err
	}
	defer rows.Close()

	var reservations []*models.Reservation

	for rows.Next() {
		r := &models.Reservation{}
		err = rows.Scan(
			&r.ID,
			&r.ClientID,
			&r.TableID,
			&r.EmployeeID,
			&r.QtdPeople,
			&r.Obs,
			&r.Date,
			&r.CreatedAt,
			&r.UpdatedAt,
		)
		if err != nil {
			m.App.ErrorLog.Println(err)
			return nil, err
		}

		reservations = append(reservations, r)
	}

	if err = rows.Err(); err != nil {
		m.App.ErrorLog.Println(err)
		return nil, err
	}

	return reservations, nil
}

// CreateReservation creates a new reservation
func (m *postgresDBRepo) CreateReservation(r *models.Reservation) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `INSERT INTO reservations (client_id, table_id, employee_id, qtd_people, obs, date, created_at, updated_at)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`

	_, err := m.DB.ExecContext(ctx, stmt, r.ClientID, r.TableID, r.EmployeeID, r.QtdPeople, r.Obs, r.Date, time.Now(), time.Now())
	if err != nil {
		m.App.ErrorLog.Println(err)
		return err
	}

	return nil
}

// GetReservationsByTableIDAndDate gets all reservations by table id and date
func (m *postgresDBRepo) GetReservationsByTableIDAndDate(tableID int, date string) ([]*models.Reservation, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `SELECT id, client_id, table_id, employee_id, qtd_people, obs, date, created_at, updated_at
	FROM reservations WHERE table_id = $1 AND date = $2`

	rows, err := m.DB.QueryContext(ctx, stmt, tableID, date)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return nil, err
	}
	defer rows.Close()

	var reservations []*models.Reservation

	for rows.Next() {
		r := &models.Reservation{}
		err = rows.Scan(
			&r.ID,
			&r.ClientID,
			&r.TableID,
			&r.EmployeeID,
			&r.QtdPeople,
			&r.Obs,
			&r.Date,
			&r.CreatedAt,
			&r.UpdatedAt,
		)
		if err != nil {
			m.App.ErrorLog.Println(err)
			return nil, err
		}

		reservations = append(reservations, r)
	}

	if err = rows.Err(); err != nil {
		m.App.ErrorLog.Println(err)
		return nil, err
	}

	return reservations, nil
}

// DeleteReservationByTableID deletes a reservation by table id
func (m *postgresDBRepo) DeleteReservationByTableID(tableID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `DELETE FROM reservations WHERE table_id = $1`

	_, err := m.DB.ExecContext(ctx, stmt, tableID)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return err
	}

	return nil
}

// DeleteReservationByID deletes a reservation by id
func (m *postgresDBRepo) DeleteReservationByID(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `DELETE FROM reservations WHERE id = $1`

	_, err := m.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return err
	}

	return nil
}

// CountReservationByDate counts the number of reservations by date
func (m *postgresDBRepo) CountReservationByDate(start string, end string) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `SELECT COUNT(id) FROM reservations WHERE date >= $1 AND date <= $2`

	var count int

	row := m.DB.QueryRowContext(ctx, stmt, start, end)
	err := row.Scan(&count)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return 0, err
	}

	return count, nil
}
