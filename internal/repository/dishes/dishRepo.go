package dishes

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

// NewPostgresRepo creates a new repository
func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.DishRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAllDishes returns all dishes
func (m *postgresDBRepo) GetAllDishes() ([]models.Dish, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT id, name, description, price, is_active, category_id, created_at, updated_at
		FROM dishes
		ORDER BY id ASC
	`

	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var dishes []models.Dish

	for rows.Next() {
		var dish models.Dish

		err := rows.Scan(
			&dish.ID,
			&dish.Name,
			&dish.Description,
			&dish.Price,
			&dish.IsActive,
			&dish.CategoryID,
			&dish.CreatedAt,
			&dish.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		dishes = append(dishes, dish)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return dishes, nil
}

// CreateDish creates a new dish
func (m *postgresDBRepo) CreateDish(dish *models.Dish) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		INSERT INTO dishes (name, description, price, is_active, category_id, created_at, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx,
		dish.Name,
		dish.Description,
		dish.Price,
		dish.IsActive,
		dish.CategoryID,
		time.Now(),
		time.Now(),
	)
	if err != nil {
		return err
	}

	return nil
}

// UpdateDish updates a dish
func (m *postgresDBRepo) UpdateDish(dish *models.Dish) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE dishes
		SET name = $1, description = $2, price = $3, updated_at = $4
		WHERE id = $5
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	_, err = stmt.ExecContext(ctx,
		dish.Name,
		dish.Description,
		dish.Price,
		time.Now(),
		dish.ID,
	)
	if err != nil {
		return err
	}

	return nil
}

// DeleteDish deletes a dish
func (m *postgresDBRepo) DeleteDish(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `DELETE FROM dishes WHERE id = $1`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}

// UpdateDishStatus updates the status of a dish
func (m *postgresDBRepo) UpdateDishStatus(id int, status bool) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE dishes
		SET is_active = $1, updated_at = $2
		WHERE id = $3
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	_, err = stmt.ExecContext(ctx,
		status,
		time.Now(),
		id,
	)
	if err != nil {
		return err
	}

	return nil
}
