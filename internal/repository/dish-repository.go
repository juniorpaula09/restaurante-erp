package repository

import "restaurante/internal/models"

type DishRepository interface {
	GetAllDishes() ([]models.Dish, error)
	CreateDish(dish *models.Dish) error
	UpdateDish(dish *models.Dish) error
	DeleteDish(id int) error
	UpdateDishStatus(id int, status bool) error
}
