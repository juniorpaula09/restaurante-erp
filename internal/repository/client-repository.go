package repository

import "restaurante/internal/models"

type ClientRepository interface {
	GetAllClients() ([]models.Client, error)
	GetClientByID(id int) (models.Client, error)
	GetClientByEmail(email string) (models.Client, error)
	CreateClient(client models.Client) error
	UpdateClient(client models.Client) error
	DeleteClient(id int) error
}
