package product

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.ProductRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// AllProducts returns all products
func (m *postgresDBRepo) AllProducts() ([]models.Product, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
	SELECT
		p.id, p.name, p.description, p.purchase_price, p.sale_price, p.category_id, p.supplier_id, p.stock, c.id, c.name, s.id, s.name, p.created_at, p.updated_at
	FROM
		products p
	LEFT JOIN
		categories c ON (p.category_id = c.id)
	LEFT JOIN
		suppliers s ON (p.supplier_id = s.id)
	ORDER BY
		p.id ASC
	`

	rows, err := m.DB.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var products []models.Product

	for rows.Next() {
		var p models.Product

		err = rows.Scan(
			&p.ID,
			&p.Name,
			&p.Description,
			&p.PurchasePrice,
			&p.SalePrice,
			&p.CategoryID,
			&p.SupplierID,
			&p.Stock,
			&p.Category.ID,
			&p.Category.Name,
			&p.Supplier.ID,
			&p.Supplier.Name,
			&p.CreatedAt,
			&p.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		products = append(products, p)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return products, nil
}

// CreateProduct creates a new product
func (m *postgresDBRepo) CreateProduct(p *models.Product) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `INSERT INTO 
		products 
			(name, description, purchase_price, sale_price, category_id, supplier_id, stock, created_at, updated_at)
		VALUES 
			($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`

	_, err := m.DB.ExecContext(ctx, stmt,
		p.Name,
		p.Description,
		p.PurchasePrice,
		p.SalePrice,
		p.CategoryID,
		p.SupplierID,
		p.Stock,
		time.Now(),
		time.Now(),
	)

	if err != nil {
		return err
	}

	return nil
}

// GetProductByID returns a product by id
func (m *postgresDBRepo) GetProductByID(id int) (models.Product, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
	SELECT
		p.id, p.name, p.description, p.purchase_price, p.sale_price, p.category_id, p.supplier_id, p.stock, c.id, c.name, s.id, s.name, p.created_at, p.updated_at
	FROM
		products p
	LEFT JOIN
		categories c ON (p.category_id = c.id)
	LEFT JOIN
		suppliers s ON (p.supplier_id = s.id)
	WHERE
		p.id = $1
	`

	row := m.DB.QueryRowContext(ctx, stmt, id)

	var p models.Product

	err := row.Scan(
		&p.ID,
		&p.Name,
		&p.Description,
		&p.PurchasePrice,
		&p.SalePrice,
		&p.CategoryID,
		&p.SupplierID,
		&p.Stock,
		&p.Category.ID,
		&p.Category.Name,
		&p.Supplier.ID,
		&p.Supplier.Name,
		&p.CreatedAt,
		&p.UpdatedAt,
	)

	if err != nil {
		return models.Product{}, err
	}

	return p, nil
}

// UpdateProduct updates a product
func (m *postgresDBRepo) UpdateProduct(p *models.Product) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
	UPDATE
		products
	SET
		name = $1, description = $2, purchase_price = $3, sale_price = $4, category_id = $5, supplier_id = $6, updated_at = $7
	WHERE
		id = $8
	`

	_, err := m.DB.ExecContext(ctx, stmt,
		p.Name,
		p.Description,
		p.PurchasePrice,
		p.SalePrice,
		p.CategoryID,
		p.SupplierID,
		time.Now(),
		p.ID,
	)

	if err != nil {
		return err
	}

	return nil
}

// DeleteProduct deletes a product
func (m *postgresDBRepo) DeleteProduct(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `DELETE FROM products WHERE id = $1`

	_, err := m.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}

	return nil
}

// GetStockByProductID returns the stock of a product
func (m *postgresDBRepo) GetStockByProductID(id int) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `SELECT stock FROM products WHERE id = $1`

	row := m.DB.QueryRowContext(ctx, stmt, id)

	var stock int

	err := row.Scan(&stock)

	if err != nil {
		return 0, err
	}

	return stock, nil
}

// UpdateProductByPurchase updates a product by purchase
func (m *postgresDBRepo) UpdateProductByPurchase(p *models.Product) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
	UPDATE
		products 
	SET
		stock = $1, purchase_price = $2, supplier_id = $3, updated_at = $4
	WHERE
		id = $5
	`

	_, err := m.DB.ExecContext(ctx, stmt,
		p.Stock,
		p.PurchasePrice,
		p.SupplierID,
		time.Now(),
		p.ID,
	)

	if err != nil {
		return err
	}

	return nil
}

// CreatePurchaseMoviments creates a new purchase moviment
func (m *postgresDBRepo) CreatePurchaseMoviments(pm *models.PurchaseMoviment) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := ` 
	INSERT INTO
		purchase_moviments
		(amount, purchase_date, user_id, supplier_id, status, created_at, updated_at, type)
	VALUES
		($1, $2, $3, $4, $5, $6, $7, $8)
	RETURNING id
	`

	var id int

	err := m.DB.QueryRowContext(ctx, stmt,
		pm.Amount,
		pm.PurchaseDate,
		pm.UserID,
		pm.SupplierID,
		pm.Status,
		time.Now(),
		time.Now(),
		pm.Type,
	).Scan(&id)

	if err != nil {
		return 0, err
	}

	return id, nil
}

// CreateBillPayable creates a new bill payable
func (m *postgresDBRepo) CreateBillPayable(bp *models.BillPayable) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
	INSERT INTO
		bill_payables
		(amount, description, user_id, purchase_moviment_id, status, due_date, bill_date, created_at, updated_at)
	VALUES
		($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`

	_, err := m.DB.ExecContext(ctx, stmt,
		bp.Amount,
		bp.Description,
		bp.UserID,
		bp.PurchaseMovimentID,
		bp.Status,
		bp.DueDate,
		bp.BillDate,
		time.Now(),
		time.Now(),
	)

	if err != nil {
		return err
	}

	return nil
}

// GetAllPurchases returns all purchases
func (m *postgresDBRepo) GetAllPurchases() ([]models.PurchaseMoviment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
	SELECT
		pm.id, pm.amount, pm.purchase_date, pm.user_id, pm.supplier_id, pm.status, pm.created_at, pm.updated_at, u.id, u.first_name, s.id, s.name
	FROM
		purchase_moviments pm
	LEFT JOIN
		users u ON (pm.user_id = u.id)
	LEFT JOIN
		suppliers s ON (pm.supplier_id = s.id)
	ORDER BY
		pm.id ASC
	`

	rows, err := m.DB.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var purchases []models.PurchaseMoviment

	for rows.Next() {
		var p models.PurchaseMoviment

		err = rows.Scan(
			&p.ID,
			&p.Amount,
			&p.PurchaseDate,
			&p.UserID,
			&p.SupplierID,
			&p.Status,
			&p.CreatedAt,
			&p.UpdatedAt,
			&p.User.ID,
			&p.User.FirstName,
			&p.Supplier.ID,
			&p.Supplier.Name,
		)
		if err != nil {
			return nil, err
		}

		purchases = append(purchases, p)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return purchases, nil
}

// DeletePurchase deletes a purchase and its bill payable
func (m *postgresDBRepo) DeletePurchase(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `DELETE FROM purchase_moviments WHERE id = $1`

	_, err := m.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}

	stmt = `DELETE FROM bill_payables WHERE purchase_moviment_id = $1`

	_, err = m.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}

	return nil
}

// GetLowStockProducts returns all products with low stock
func (m *postgresDBRepo) GetLowStockProducts(minStock int) ([]models.Product, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
	SELECT
		p.id, p.name, p.description, p.purchase_price, p.sale_price, p.category_id, p.supplier_id, p.stock, c.id, c.name, s.id, s.name, p.created_at, p.updated_at
	FROM
		products p
	LEFT JOIN
		categories c ON (p.category_id = c.id)
	LEFT JOIN
		suppliers s ON (p.supplier_id = s.id)
	WHERE
		p.stock <= $1
	ORDER BY
		p.id ASC
	`

	rows, err := m.DB.QueryContext(ctx, stmt, minStock)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var products []models.Product

	for rows.Next() {
		var p models.Product

		err = rows.Scan(
			&p.ID,
			&p.Name,
			&p.Description,
			&p.PurchasePrice,
			&p.SalePrice,
			&p.CategoryID,
			&p.SupplierID,
			&p.Stock,
			&p.Category.ID,
			&p.Category.Name,
			&p.Supplier.ID,
			&p.Supplier.Name,
			&p.CreatedAt,
			&p.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		products = append(products, p)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return products, nil
}

// GetAllPurchasesFromReport returns all purchases from report
func (m *postgresDBRepo) GetAllPurchasesFromReport(sd, ed, status string) ([]models.PurchaseMoviment, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := ` 
	SELECT	
		pm.id, pm.amount, pm.purchase_date, pm.user_id, pm.supplier_id, pm.status, pm.created_at, pm.updated_at, u.id, u.first_name, s.id, s.name
	FROM
		purchase_moviments pm
	LEFT JOIN
		users u ON (pm.user_id = u.id)
	LEFT JOIN
		suppliers s ON (pm.supplier_id = s.id)
	WHERE
		pm.purchase_date >= $1 AND pm.purchase_date <= $2 AND (pm.status LIKE $3 OR $3 = '')
	ORDER BY
		pm.id ASC
	`

	rows, err := m.DB.QueryContext(ctx, stmt, sd, ed, status)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var purchases []models.PurchaseMoviment

	for rows.Next() {
		var p models.PurchaseMoviment

		err = rows.Scan(
			&p.ID,
			&p.Amount,
			&p.PurchaseDate,
			&p.UserID,
			&p.SupplierID,
			&p.Status,
			&p.CreatedAt,
			&p.UpdatedAt,
			&p.User.ID,
			&p.User.FirstName,
			&p.Supplier.ID,
			&p.Supplier.Name,
		)
		if err != nil {
			return nil, err
		}

		purchases = append(purchases, p)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return purchases, nil
}
