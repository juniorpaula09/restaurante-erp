package repository

import "restaurante/internal/models"

type SupplierRepository interface {
	GetAll() ([]*models.Supplier, error)
	CreateSupplier(s *models.Supplier) error
	GetSupplierByEmail(email string) (*models.Supplier, error)
	GetSupplierByID(id int) (*models.Supplier, error)
	UpdateSupplier(id int, s *models.Supplier) error
	DeleteSupplier(id int) error
}
