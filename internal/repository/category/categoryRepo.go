package category

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.CategoryRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAllCategories returns all categories
func (m *postgresDBRepo) GetAllCategories() ([]*models.Category, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `SELECT id, name, created_at, updated_at FROM categories ORDER BY id ASC`

	rows, err := m.DB.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var categories []*models.Category

	for rows.Next() {
		var c models.Category

		err := rows.Scan(
			&c.ID,
			&c.Name,
			&c.CreatedAt,
			&c.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		categories = append(categories, &c)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return categories, nil
}

// CreateCategory creates a new category
func (m *postgresDBRepo) CreateCategory(c *models.Category) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `INSERT INTO categories (name, created_at, updated_at) VALUES ($1, $2, $3)`

	_, err := m.DB.ExecContext(ctx, stmt, c.Name, time.Now(), time.Now())
	if err != nil {
		return err
	}

	return nil
}

// UpdateCategory updates a category
func (m *postgresDBRepo) UpdateCategory(c *models.Category) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `UPDATE categories SET name = $1, updated_at = $2 WHERE id = $3`

	_, err := m.DB.ExecContext(ctx, stmt, c.Name, time.Now(), c.ID)
	if err != nil {
		return err
	}

	return nil
}

// DeleteCategory deletes a category
func (m *postgresDBRepo) DeleteCategory(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `DELETE FROM categories WHERE id = $1`

	_, err := m.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}

	return nil
}
