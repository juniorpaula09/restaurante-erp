package repository

import "restaurante/internal/models"

type ProductRepository interface {
	AllProducts() ([]models.Product, error)
	CreateProduct(p *models.Product) error
	GetProductByID(id int) (models.Product, error)
	UpdateProduct(p *models.Product) error
	DeleteProduct(id int) error
	GetStockByProductID(id int) (int, error)
	UpdateProductByPurchase(p *models.Product) error
	CreatePurchaseMoviments(pm *models.PurchaseMoviment) (int, error)
	CreateBillPayable(bp *models.BillPayable) error
	GetAllPurchases() ([]models.PurchaseMoviment, error)
	DeletePurchase(id int) error
	GetLowStockProducts(minStock int) ([]models.Product, error)
	GetAllPurchasesFromReport(sd, ed, status string) ([]models.PurchaseMoviment, error)
}
