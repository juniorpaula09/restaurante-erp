package employeers

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.EmployeeRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAllEmployees returns all employees from the database
func (m *postgresDBRepo) GetAllEmployees() ([]*models.Employee, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT id, first_name, last_name, email, cpf, phone, address, access_level, created_at, updated_at
		FROM employees
		ORDER BY ID ASC
	`

	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var employees []*models.Employee

	for rows.Next() {
		var e models.Employee

		err := rows.Scan(
			&e.ID,
			&e.FirstName,
			&e.LastName,
			&e.Email,
			&e.CPF,
			&e.Phone,
			&e.Address,
			&e.AccessLevel,
			&e.CreatedAt,
			&e.UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		employees = append(employees, &e)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return employees, nil
}

// GetEmployeeByID returns an employee by its id
func (m *postgresDBRepo) GetEmployeeByID(id int) (*models.Employee, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT id, first_name, last_name, email, cpf, phone, address, access_level, created_at, updated_at
		FROM employees
		WHERE id = $1
	`

	row := m.DB.QueryRowContext(ctx, query, id)

	var employee models.Employee

	err := row.Scan(
		&employee.ID,
		&employee.FirstName,
		&employee.LastName,
		&employee.Email,
		&employee.CPF,
		&employee.Phone,
		&employee.Address,
		&employee.AccessLevel,
		&employee.CreatedAt,
		&employee.UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &employee, nil
}

// UpdateEmployee updates an employee in the database
func (m *postgresDBRepo) UpdateEmployee(employee *models.Employee) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE employees
		SET first_name = $1, last_name = $2, email = $3, cpf = $4, phone = $5, address = $6, access_level = $7, updated_at = now()
		WHERE id = $8
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx,
		employee.FirstName,
		employee.LastName,
		employee.Email,
		employee.CPF,
		employee.Phone,
		employee.Address,
		employee.AccessLevel,
		employee.ID,
	)

	if err != nil {
		return err
	}

	return nil
}

// DeleteEmployee deletes an employee from the database
func (m *postgresDBRepo) DeleteEmployee(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		DELETE FROM employees
		WHERE id = $1
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}

// CreateEmployee creates a new employee in the database
func (m *postgresDBRepo) CreateEmployee(employee *models.Employee) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		INSERT INTO employees (first_name, last_name, email, cpf, phone, address, access_level, created_at, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, now(), now())
		RETURNING id
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRowContext(ctx,
		employee.FirstName,
		employee.LastName,
		employee.Email,
		employee.CPF,
		employee.Phone,
		employee.Address,
		employee.AccessLevel,
	).Scan(&employee.ID)

	if err != nil {
		return err
	}

	return nil
}

// GetPositions returns all positions from the database
func (m *postgresDBRepo) GetPositions() ([]*models.Position, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		SELECT id, name, access_level
		FROM positions
		ORDER BY ID ASC
	`

	rows, err := m.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var positions []*models.Position

	for rows.Next() {
		var p models.Position

		err := rows.Scan(
			&p.ID,
			&p.Name,
			&p.AccessLevel,
		)

		if err != nil {
			return nil, err
		}

		positions = append(positions, &p)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return positions, nil
}

// CreatePosition creates a new position in the database
func (m *postgresDBRepo) CreatePosition(position *models.Position) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		INSERT INTO positions (name, access_level, created_at, updated_at)
		VALUES ($1, $2, now(), now())
		RETURNING id
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRowContext(ctx, position.Name, position.AccessLevel).Scan(&position.ID)
	if err != nil {
		return err
	}

	return nil
}

// UpdatePosition updates a position in the database
func (m *postgresDBRepo) UpdatePosition(position *models.Position) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		UPDATE positions
		SET name = $1, access_level = $2, updated_at = now()
		WHERE id = $3
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, position.Name, position.AccessLevel, position.ID)
	if err != nil {
		return err
	}

	return nil
}

// DeletePosition deletes a position from the database
func (m *postgresDBRepo) DeletePosition(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	query := `
		DELETE FROM positions
		WHERE id = $1
	`

	stmt, err := m.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
