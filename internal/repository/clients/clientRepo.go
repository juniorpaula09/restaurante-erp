package clients

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

// NewPostgresRepo creates a new repository
func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.ClientRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAllClients gets all clients
func (m *postgresDBRepo) GetAllClients() ([]models.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var clients []models.Client

	stmt := `
		SELECT
			id, name, email, phone, created_at, updated_at
		FROM
			clients
	`

	rows, err := m.DB.QueryContext(ctx, stmt)
	if err != nil {
		return clients, err
	}
	defer rows.Close()

	for rows.Next() {
		var client models.Client
		err := rows.Scan(&client.ID, &client.Name, &client.Email, &client.Phone, &client.CreatedAt, &client.UpdatedAt)
		if err != nil {
			return clients, err
		}

		clients = append(clients, client)
	}

	if err = rows.Err(); err != nil {
		return clients, err
	}

	return clients, nil
}

// CreateClient creates a new client
func (m *postgresDBRepo) CreateClient(client models.Client) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
		INSERT INTO 
			clients (name, email, phone, created_at, updated_at)
		VALUES 
			($1, $2, $3, $4, $5)
	`

	_, err := m.DB.ExecContext(ctx, stmt, client.Name, client.Email, client.Phone, time.Now(), time.Now())
	if err != nil {
		return err
	}

	return nil
}

// GetClientByEmail gets a client by email
func (m *postgresDBRepo) GetClientByEmail(email string) (models.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var client models.Client

	stmt := `
		SELECT
			id, name, email, phone, created_at, updated_at
		FROM
			clients
		WHERE
			email = $1
	`

	row := m.DB.QueryRowContext(ctx, stmt, email)
	err := row.Scan(&client.ID, &client.Name, &client.Email, &client.Phone, &client.CreatedAt, &client.UpdatedAt)
	if err != nil {
		return client, err
	}

	return client, nil
}

// GetClientByID gets a client by id
func (m *postgresDBRepo) GetClientByID(id int) (models.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var client models.Client

	stmt := `
		SELECT
			id, name, email, phone, created_at, updated_at
		FROM
			clients
		WHERE
			id = $1
	`

	row := m.DB.QueryRowContext(ctx, stmt, id)
	err := row.Scan(&client.ID, &client.Name, &client.Email, &client.Phone, &client.CreatedAt, &client.UpdatedAt)
	if err != nil {
		return client, err
	}

	return client, nil
}

// UpdateClient updates a client
func (m *postgresDBRepo) UpdateClient(client models.Client) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
		UPDATE
			clients
		SET
			name = $1, email = $2, phone = $3, updated_at = $4
		WHERE
			id = $5
	`

	_, err := m.DB.ExecContext(ctx, stmt, client.Name, client.Email, client.Phone, time.Now(), client.ID)
	if err != nil {
		return err
	}

	return nil
}

// DeleteClient deletes a client
func (m *postgresDBRepo) DeleteClient(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
		DELETE FROM
			clients
		WHERE
			id = $1
	`

	_, err := m.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}

	return nil
}
