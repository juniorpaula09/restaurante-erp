package supplier

import (
	"context"
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository"
	"time"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.SupplierRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}

// GetAll gets all suppliers from the database
func (m *postgresDBRepo) GetAll() ([]*models.Supplier, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var suppliers []*models.Supplier

	stmt := `
		SELECT
			id, name, email, phone, product_type, created_at, updated_at
		FROM
			suppliers
	`

	rows, err := m.DB.QueryContext(ctx, stmt)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var supplier models.Supplier

		err := rows.Scan(
			&supplier.ID,
			&supplier.Name,
			&supplier.Email,
			&supplier.Phone,
			&supplier.ProductType,
			&supplier.CreatedAt,
			&supplier.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}

		suppliers = append(suppliers, &supplier)
	}

	return suppliers, nil
}

// CreateSupplier creates a new supplier in the database
func (m *postgresDBRepo) CreateSupplier(s *models.Supplier) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
		INSERT INTO 
			suppliers (name, email, phone, product_type, created_at, updated_at) 
		VALUES ($1, $2, $3, $4, $5, $6)
	`

	_, err := m.DB.ExecContext(ctx, stmt, s.Name, s.Email, s.Phone, s.ProductType, time.Now(), time.Now())
	if err != nil {
		return err
	}

	return nil
}

// GetSupplierByID gets a supplier by its id
func (m *postgresDBRepo) GetSupplierByEmail(email string) (*models.Supplier, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var supplier models.Supplier

	stmt := `
		SELECT
			id, name, email, phone, product_type, created_at, updated_at
		FROM
			suppliers
		WHERE
			email = $1
	`

	row := m.DB.QueryRowContext(ctx, stmt, email)

	err := row.Scan(
		&supplier.ID,
		&supplier.Name,
		&supplier.Email,
		&supplier.Phone,
		&supplier.ProductType,
		&supplier.CreatedAt,
		&supplier.UpdatedAt,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return &supplier, nil
}

// GetSupplierByID gets a supplier by its id
func (m *postgresDBRepo) GetSupplierByID(id int) (*models.Supplier, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var supplier models.Supplier

	stmt := `
		SELECT
			id, name, email, phone, product_type, created_at, updated_at
		FROM
			suppliers
		WHERE
			id = $1
	`

	row := m.DB.QueryRowContext(ctx, stmt, id)

	err := row.Scan(
		&supplier.ID,
		&supplier.Name,
		&supplier.Email,
		&supplier.Phone,
		&supplier.ProductType,
		&supplier.CreatedAt,
		&supplier.UpdatedAt,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return &supplier, nil
}

// UpdateSupplier updates a supplier in the database
func (m *postgresDBRepo) UpdateSupplier(id int, s *models.Supplier) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
		UPDATE
			suppliers
		SET
			name = $1, email = $2, phone = $3, product_type = $4, updated_at = $5
		WHERE
			id = $6
	`

	_, err := m.DB.ExecContext(ctx, stmt, s.Name, s.Email, s.Phone, s.ProductType, time.Now(), id)
	if err != nil {
		return err
	}

	return nil
}

// DeleteSupplier deletes a supplier from the database
func (m *postgresDBRepo) DeleteSupplier(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	stmt := `
		DELETE FROM
			suppliers
		WHERE
			id = $1
	`

	_, err := m.DB.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}

	return nil
}
