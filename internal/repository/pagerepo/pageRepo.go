package pagerepo

import (
	"database/sql"
	"restaurante/internal/config"
	"restaurante/internal/repository"
)

type postgresDBRepo struct {
	App *config.Application
	DB  *sql.DB
}

// NewPostgresRepo creates a new repository
func NewPostgresRepo(conn *sql.DB, a *config.Application) repository.PageRepository {
	return &postgresDBRepo{
		App: a,
		DB:  conn,
	}
}
