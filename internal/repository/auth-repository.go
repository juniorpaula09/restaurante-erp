package repository

import (
	"restaurante/internal/models"
	"time"
)

type AuthRepository interface {
	GenerateToken(userID int, ttl time.Duration, scope string) (*models.Token, error)
	InsertToken(t *models.Token, u models.User) error
}
