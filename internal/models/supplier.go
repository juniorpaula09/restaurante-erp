package models

import (
	"reflect"
	"restaurante/internal/validation"
)

// Supplier is the model used for the supplier table in the database
type Supplier struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Email       string `json:"email"`
	Phone       string `json:"phone"`
	ProductType string `json:"product_type"`

	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

var supplierValidationRules = map[string]string{
	"Name":        "nome não pode ser vazio",
	"Email":       "email não pode ser vazio",
	"Phone":       "telefone não pode ser vazio",
	"ProductType": "tipo de produto não pode ser vazio",
}

// ValidateFields validate the fields of a supplier struct
func (s *Supplier) ValidateFields(field Supplier) string {
	for fieldName, errorMessage := range supplierValidationRules {
		value := s.getFieldByName(field, fieldName)
		if value == "" {
			return errorMessage
		}
		if fieldName == "Email" && !validation.IsEmail(value) {
			return "email inválido"
		}
	}

	return ""
}

// getFieldByName return the value of a field from a struct
func (s Supplier) getFieldByName(field Supplier, fieldName string) string {
	r := reflect.ValueOf(field)
	f := reflect.Indirect(r).FieldByName(fieldName)
	return f.String()
}
