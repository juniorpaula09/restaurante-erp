package models

import "time"

type Reservation struct {
	ID         int     `json:"id"`
	ClientID   int     `json:"client_id"`
	TableID    int     `json:"table_id"`
	EmployeeID int     `json:"employee_id,omitempty"`
	QtdPeople  int     `json:"qtd_people,omitempty"`
	Obs        string  `json:"obs,omitempty"`
	Date       string  `json:"date"`
	Client     *Client `json:"client,omitempty"`
	Table      *Table  `json:"table,omitempty"`
	Employee   *User   `json:"employee,omitempty"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (r *Reservation) ConvertDate() string {
	date := r.Date
	return date[8:10] + "/" + date[5:7] + "/" + date[0:4]
}
