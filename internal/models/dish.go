package models

type Dish struct {
	ID          int     `json:"id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Price       float64 `json:"price"`
	IsActive    bool    `json:"is_active,omitempty"`
	CategoryID  int     `json:"category_id"`

	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func (d *Dish) ValidateFields(dish Dish) string {
	if dish.Name == "" {
		return "Nome do prato não pode ser vazio"
	}
	if dish.Description == "" {
		return "Descrição do prato não pode ser vazio"
	}
	if dish.Price == 0 {
		return "Preço do prato não pode ser vazio"
	}
	if dish.Price < 0 {
		return "Preço do prato não pode ser negativo"
	}
	if dish.CategoryID == 0 {
		return "Categoria do prato não pode ser vazio"
	}

	return ""
}
