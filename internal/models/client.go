package models

import (
	"reflect"
	"restaurante/internal/validation"
)

type Client struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Phone string `json:"phone"`

	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

var clientValidationRules = map[string]string{
	"Name":  "nome não pode ser vazio",
	"Email": "email não pode ser vazio",
	"Phone": "telefone não pode ser vazio",
}

// ValidateFields validate the fields of a client struct
func (c *Client) ValidateFields(field Client) string {
	for fieldName, errorMessage := range clientValidationRules {
		value := c.getFieldByName(field, fieldName)
		if value == "" {
			return errorMessage
		}
		if fieldName == "Email" && !validation.IsEmail(value) {
			return "email inválido"
		}
		if fieldName == "Phone" && len(value) != 11 {
			return "Telefone deve ter 11 caracteres"
		}
	}

	return ""
}

// getFieldByName return the value of a field from a struct
func (c Client) getFieldByName(field Client, fieldName string) string {
	r := reflect.ValueOf(field)
	f := reflect.Indirect(r).FieldByName(fieldName)
	return f.String()
}
