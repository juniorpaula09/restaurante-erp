package models

import (
	"reflect"
	"time"
)

type Table struct {
	ID           int            `json:"id"`
	Name         string         `json:"name"`
	Description  string         `json:"description"`
	Reservations []*Reservation `json:"reservations,omitempty"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

var tableValidationRules = map[string]string{
	"Name":        "nome não pode ser vazio",
	"Description": "descrição não pode ser vazio",
}

// ValidateFields validate the fields of a table struct
func (t *Table) ValidateFields(field Table) string {
	for fieldName, errorMessage := range tableValidationRules {
		value := t.getFieldByName(field, fieldName)
		if value == "" {
			return errorMessage
		}
	}

	return ""
}

// getFieldByName return the value of a field from a struct
func (t Table) getFieldByName(field Table, fieldName string) string {
	r := reflect.ValueOf(field)
	f := reflect.Indirect(r).FieldByName(fieldName)
	return f.String()
}
