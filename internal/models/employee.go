package models

import (
	"reflect"
	"restaurante/internal/validation"
	"time"
)

type Employee struct {
	ID          int    `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	CPF         string `json:"cpf"`
	Phone       string `json:"phone"`
	Address     string `json:"address"`
	AccessLevel int    `json:"access_level"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Position struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	AccessLevel int    `json:"access_level"`
}

type InputEmployee struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	CPF         string `json:"cpf"`
	Phone       string `json:"phone"`
	Address     string `json:"address"`
	AccessLevel int    `json:"access_level"`
}

var validationRules = map[string]string{
	"FirstName":   "nome não pode ser vazio",
	"LastName":    "sobrenome não pode ser vazio",
	"Email":       "email não pode ser vazio",
	"CPF":         "cpf não pode ser vazio",
	"Phone":       "telefone não pode ser vazio",
	"Address":     "endereço não pode ser vazio",
	"AccessLevel": "nível de acesso não pode ser vazio",
}

// ValidateFields validate the fields of a employee
func (e *Employee) ValidateFields(field InputEmployee) string {
	for fieldName, errorMessage := range validationRules {
		value := getFieldByName(field, fieldName)
		if value == "" {
			return errorMessage
		}

		if fieldName == "Email" && !validation.IsEmail(value) {
			return "email inválido"
		}
		if fieldName == "CPF" && len(value) != 11 {
			return "CPF deve ter 11 caracteres"
		}
		if fieldName == "Phone" && len(value) != 11 {
			return "Telefone deve ter 11 caracteres"
		}
	}

	return ""
}

// getFieldByName return the value of a field from a struct
func getFieldByName(field InputEmployee, fieldName string) string {
	r := reflect.ValueOf(field)
	f := reflect.Indirect(r).FieldByName(fieldName)
	return f.String()
}
