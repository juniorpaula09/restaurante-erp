package models

// Category is the model for the category table
type Category struct {
	ID   int    `json:"id"`
	Name string `json:"name"`

	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}
