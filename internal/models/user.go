package models

import (
	"reflect"
	"restaurante/internal/validation"
)

type User struct {
	ID          int    `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Email       string `json:"email"`
	Password    string `json:"password"`
	AccessLevel int    `json:"access_level"`

	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

var userValidationRules = map[string]string{
	"FirstName":   "nome não pode ser vazio",
	"LastName":    "sobrenome não pode ser vazio",
	"Email":       "email não pode ser vazio",
	"Password":    "senha não pode ser vazio",
	"AccessLevel": "cargo não pode ser vazio",
}

// ValidateFields validate the fields of a user struct
func (u *User) ValidateFields(field User) string {
	for fieldName, errorMessage := range userValidationRules {
		value := u.getFieldByName(field, fieldName)
		if value == "" {
			return errorMessage
		}

		if fieldName == "Email" && !validation.IsEmail(value) {
			return "email inválido"
		}
		if fieldName == "Password" && len(value) < 6 {
			return "senha deve ter no mínimo 6 caracteres"
		}
	}

	return ""
}

// getFieldByName return the value of a field from a struct
func (u User) getFieldByName(field User, fieldName string) string {
	r := reflect.ValueOf(field)
	f := reflect.Indirect(r).FieldByName(fieldName)
	return f.String()
}
