package models

import "time"

type PurchaseMoviment struct {
	ID           int       `json:"id"`
	Amount       float64   `json:"amount"`
	PurchaseDate time.Time `json:"purchase_date"`
	UserID       int       `json:"user_id"`
	SupplierID   int       `json:"supplier_id"`
	Status       string    `json:"status"`
	Type         string    `json:"type"`
	User         User      `json:"user"`
	Supplier     Supplier  `json:"supplier"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type BillPayable struct {
	ID                 int       `json:"id"`
	Amount             float64   `json:"amount"`
	Description        string    `json:"description"`
	UserID             int       `json:"user_id"`
	PurchaseMovimentID int       `json:"purchase_moviment_id"`
	Status             string    `json:"status"`
	DueDate            time.Time `json:"due_date"`
	BillDate           time.Time `json:"bill_date"`
	User               User      `json:"user"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type BillReceive struct {
	ID                 int       `json:"id"`
	Amount             float64   `json:"amount"`
	Description        string    `json:"description"`
	UserID             int       `json:"user_id"`
	PurchaseMovimentID int       `json:"purchase_moviment_id"`
	Status             string    `json:"status"`
	DueDate            time.Time `json:"due_date"`
	BillDate           time.Time `json:"bill_date"`
	User               User      `json:"user"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
