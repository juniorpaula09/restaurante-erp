package models

import (
	"reflect"
	"time"
)

// Product is the model for the product table
type Product struct {
	ID            int      `json:"id"`
	Name          string   `json:"name"`
	Description   string   `json:"description"`
	PurchasePrice float64  `json:"purchase_price"`
	SalePrice     float64  `json:"sale_price"`
	CategoryID    int      `json:"category_id"`
	SupplierID    int      `json:"supplier_id"`
	Stock         int      `json:"stock"`
	Category      Category `json:"category"`
	Supplier      Supplier `json:"supplier"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

var productRules = map[string]string{
	"Name":        "nome não pode ser vazio",
	"Description": "descrição não pode ser vazio",
	"SalePrice":   "preço de venda não pode ser vazio",
	"CategoryID":  "categoria não pode ser vazio",
}

// ValidateFields validate the fields of a product struct
func (p *Product) ValidateFields(field Product) string {
	for fieldName, errorMessage := range productRules {
		value := p.getFieldByName(field, fieldName)
		if value == "" {
			return errorMessage
		}

		if fieldName == "PurchasePrice" || fieldName == "SalePrice" {
			if field.SalePrice <= 0 || !p.isFloat(field.SalePrice) {
				return "preço da venda inválido"
			}
		}

		if fieldName == "CategoryID" {
			if field.CategoryID <= 0 {
				return "selecione uma categoria"
			}
		}
	}

	return ""
}

// isFloat check if a value is float
func (p *Product) isFloat(value interface{}) bool {
	switch value.(type) {
	case float32, float64:
		return true
	}
	return false
}

// getFieldByName return the value of a field from a struct
func (p Product) getFieldByName(field Product, fieldName string) string {
	r := reflect.ValueOf(field)
	f := reflect.Indirect(r).FieldByName(fieldName)
	return f.String()
}
