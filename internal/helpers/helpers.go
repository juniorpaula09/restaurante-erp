package helpers

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"restaurante/internal/config"
	"strings"

	"github.com/asaskevich/govalidator"
	"golang.org/x/crypto/bcrypt"
)

var app *config.Application

// NewHelpers is a constructor for the helpers package
// It sets the app variable to the application passed in
// This allows the helpers package to access the application
func NewHelpers(a *config.Application) {
	app = a
}

// IsAuthenticated checks if the user is authenticated
// It returns true if the user is authenticated
// It returns false if the user is not authenticated
func IsAuthenticated(r *http.Request) bool {
	exists := app.Session.Exists(r.Context(), "user_id")
	return exists
}

// AuthenticateToken checks if the token is valid and returns token if valid
func AuthenticateToken(r *http.Request) (string, error) {
	// get the token from the request header
	authorizationToken := r.Header.Get("Authorization")
	if authorizationToken == "" {
		return "", errors.New("missing authorization token")
	}

	headerParts := strings.Split(authorizationToken, " ")
	if len(headerParts) != 2 || headerParts[0] != "Bearer" {
		return "", errors.New("authorization header format musb be Bearer {token}")
	}

	token := headerParts[1]
	if len(token) != 26 {
		return "", errors.New("invalid authorization token")
	}

	return token, nil
}

// isAdmin checks if the user is an admin
// It returns true if the user is an admin
// It returns false if the user is not an admin
func IsAdmin(r *http.Request) bool {
	accessLevel := app.Session.GetInt(r.Context(), "access_level")

	return accessLevel == 1
}

// IsManager checks if the user is a manager
// It returns true if the user is a manager
// It returns false if the user is not a manager
func IsManager(r *http.Request) bool {
	accessLevel := app.Session.GetInt(r.Context(), "access_level")

	return accessLevel == 1 || accessLevel == 2
}

// WriteJSON writes the given interface as JSON to the response writer
func WriteJSON(w http.ResponseWriter, status int, data interface{}, headers ...http.Header) error {
	out, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Println("Error to marshal data", err)
		return err
	}

	if len(headers) > 0 {
		for key, value := range headers[0] {
			w.Header().Set(key, value[0])
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(out)

	return nil
}

// ReadJSON reads the JSON from the request body and unmarshals it into the given interface
func ReadJSON(w http.ResponseWriter, r *http.Request, data interface{}) error {
	maxBytes := 1_048_576 // 1MB

	r.Body = http.MaxBytesReader(w, r.Body, int64(maxBytes))

	dec := json.NewDecoder(r.Body)
	err := dec.Decode(data)
	if err != nil {
		log.Println("Error to decode data", err)
		return err
	}

	err = dec.Decode(&struct{}{})
	if err != io.EOF {
		return errors.New("body must only have a single JSON value")
	}

	return nil
}

// passwordMatch checks if the given password matches the given hash
func PasswordMatch(hash, password string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		switch {
		case errors.Is(err, bcrypt.ErrMismatchedHashAndPassword):
			return false, nil
		default:
			return false, err
		}
	}
	return true, nil
}

// HashPassword hashes the given password
func HashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		log.Println("Error to generate hash", err)
		return "", err
	}

	return string(hash), nil
}

// Unauthorized is a helper function to send a 401 status and JSON response
func Unauthorized(w http.ResponseWriter, err error) error {
	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	payload.Error = true
	payload.Message = err.Error()

	out, err := json.MarshalIndent(payload, "", "\t")
	if err != nil {
		log.Println("Error to marshal data", err)
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusUnauthorized)
	w.Write(out)

	return nil
}

// BadRequest is a helper function to send a 400 status and JSON response
func BadRequest(w http.ResponseWriter, r *http.Request, err error) error {
	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	payload.Error = true
	payload.Message = err.Error()

	out, err := json.MarshalIndent(payload, "", "\t")
	if err != nil {
		log.Println("Error to marshal data", err)
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	w.Write(out)

	return nil
}

// NotFound is a helper function to send a 404 status and JSON response
func NotFound(w http.ResponseWriter, r *http.Request, err error) error {
	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	payload.Error = true
	payload.Message = err.Error()

	out, err := json.MarshalIndent(payload, "", "\t")
	if err != nil {
		log.Println("Error to marshal data", err)
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	w.Write(out)

	return nil
}

// ServerError is a helper function to send a 500 status and JSON response
func ServerError(w http.ResponseWriter, r *http.Request, err error) error {
	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	payload.Error = true
	payload.Message = err.Error()

	out, err := json.MarshalIndent(payload, "", "\t")
	if err != nil {
		log.Println("Error to marshal data", err)
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	w.Write(out)

	return nil
}

// RemoveCPFMask removes the mask from the CPF
func RemoveCPFMask(cpf string) string {
	re := strings.NewReplacer(".", "", "-", "")
	return re.Replace(cpf)
}

// RemovePhoneMask removes the mask from the phone
func RemovePhoneMask(phone string) string {
	re := strings.NewReplacer("(", "", ")", "", "-", "", " ", "")
	return re.Replace(phone)
}

// VerifyEmail checks if the email is valid
func VerifyEmail(email string) bool {
	return govalidator.IsEmail(email)
}
