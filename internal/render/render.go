package render

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"restaurante/internal/config"
	"restaurante/internal/models"
	"restaurante/internal/repository/users"
	"strconv"
	"strings"
	"time"

	"github.com/justinas/nosurf"
)

var functions = template.FuncMap{
	"convertAccessLevelToString": ConvertAccessLevelToString,
	"formatDate":                 FormatDate,
	"formatDateString":           FormatDateString,
	"formatCurrency":             FormatCurrency,
	"checkLowStock":              CheckLowStock,
}

var app *config.Application
var db *sql.DB

func NewRenderer(a *config.Application, conn *sql.DB) {
	app = a
	db = conn
}

// FormatDate formats a date to a string
func FormatDate(t time.Time) string {
	return t.Format("02/01/2006")
}

// FormatDateString formats a date according to the given format
func FormatDateString(t time.Time, f string) string {
	return t.Format(f)
}

// FormatCurrency formats a float64 to a string
func FormatCurrency(v float64) string {
	return fmt.Sprintf("%.2f", v)
}

// CheckLowStock checks if the stock is lower than the minimum stock
func CheckLowStock(stock int) bool {
	minStock := os.Getenv("STOCK_MIN")
	min, _ := strconv.Atoi(minStock)

	return stock < min
}

// ConvertAccessLevelToString converts an access level to a string
func ConvertAccessLevelToString(accessLevel int) string {
	switch accessLevel {
	case 1:
		return "Administrador"
	case 2:
		return "Operador"
	case 3:
		return "Recepção"
	case 4:
		return "Garçon"
	case 5:
		return "Chefe"
	default:
		return "Desconhecido"
	}
}

// ConvertPositionToAccessLevel converts a position to an access level
func ConvertPositionToAccessLevel(position string) int {
	switch strings.ToLower(position) {
	case "administrador":
		return 1
	case "operador":
		return 2
	case "recepção":
		return 3
	case "garçon":
		return 4
	case "chefe":
		return 5
	default:
		return 0
	}
}

func GetUserById(id int, db *sql.DB) (*models.User, error) {
	userRepo := users.NewPostgresRepo(db, app)
	userRepo.GetUserByID(id)

	user, err := userRepo.GetUserByID(id)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// AddDefaultData adds data for all templates
func AddDefaultData(td *models.TemplateData, w http.ResponseWriter, r *http.Request) *models.TemplateData {
	td.Flash = app.Session.PopString(r.Context(), "flash")
	td.Warning = app.Session.PopString(r.Context(), "warning")
	td.Error = app.Session.PopString(r.Context(), "error")
	td.CSRFToken = nosurf.Token(r)

	// check if user is authenticated. If they are, add user data to template data
	if app.Session.Exists(r.Context(), "user_id") {
		td.IsAuthenticated = 1
		user, err := GetUserById(app.Session.GetInt(r.Context(), "user_id"), db)
		if err != nil {
			app.ErrorLog.Println("Could not get user by id", err)
			app.Session.Destroy(r.Context())
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}

		td.User = user

	} else {
		td.IsAuthenticated = 0
	}

	return td
}

func Template(w http.ResponseWriter, r *http.Request, tmpl string, td *models.TemplateData) error {
	var tc map[string]*template.Template

	// Get the template cache from the app config
	if app.UseCache {
		tc = app.TemplateCache
	} else {
		tc, _ = CreateTemplateCache()
	}

	t, ok := tc[tmpl]
	if !ok {
		app.ErrorLog.Println("Could not get template from template cache")
		return errors.New("could not get template from template cache")
	}

	buf := new(bytes.Buffer)

	td = AddDefaultData(td, w, r)

	_ = t.Execute(buf, td)

	_, err := buf.WriteTo(w)
	if err != nil {
		app.ErrorLog.Println("Could not write template to browser", err)
		return err
	}

	return nil
}

func CreateTemplateCache() (map[string]*template.Template, error) {
	mcache := map[string]*template.Template{}

	// get all page templates
	pages, err := filepath.Glob("./templates/*.page.tmpl")
	if err != nil {
		return mcache, err
	}

	// loop through pages
	for _, page := range pages {
		name := filepath.Base(page)
		ts, err := template.New(name).Funcs(functions).ParseFiles(page)
		if err != nil {
			return mcache, err
		}

		matches, err := filepath.Glob("./templates/*.layout.tmpl")
		if err != nil {
			return mcache, err
		}

		// if there are any matches, parse them
		if len(matches) > 0 {
			ts, err = ts.ParseGlob("./templates/*.layout.tmpl")
			if err != nil {
				return mcache, err
			}
		}

		mcache[name] = ts
	}

	return mcache, nil
}
