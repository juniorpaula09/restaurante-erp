package handlers

import (
	"errors"
	"log"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/tables"
	"strconv"

	"github.com/go-chi/chi"
)

// TableRepo the repository used by the handlers
var TableRepo *TableRepository

// TableRepository is the repository type
type TableRepository struct {
	App     *config.Application
	TableDB repository.TableRepository
}

// NewTableRepo sets the repository for the handlers
func NewTableRepo(a *config.Application, db *driver.DB) *TableRepository {
	return &TableRepository{
		App:     a,
		TableDB: tables.NewPostgresRepo(db.SQL, a),
	}
}

// NewTableHandlers sets the repository for the handlers
func NewTableHandlers(r *TableRepository) {
	TableRepo = r
}

// TablesPage is the handler used to render the tables page
func (m *TableRepository) TablesPage(w http.ResponseWriter, r *http.Request) {
	tables, err := m.TableDB.AllTables()
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "Error ao buscar as mesas")
		return
	}

	var data = make(map[string]interface{})
	data["tables"] = tables

	render.Template(w, r, "tables.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateTable is the handler used to receive a post request to create a table
func (m *TableRepository) CreateTable(w http.ResponseWriter, r *http.Request) {
	var inputTable models.Table

	err := helpers.ReadJSON(w, r, &inputTable)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	msg := inputTable.ValidateFields(inputTable)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}

	err = m.TableDB.CreateTable(&inputTable)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Mesa criada com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdateTable is the handler used to receive a put request to update a table
func (m *TableRepository) UpdateTable(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	tableId, _ := strconv.Atoi(id)

	var inputTable models.Table

	err := helpers.ReadJSON(w, r, &inputTable)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	err = m.TableDB.UpdateTable(tableId, &inputTable)
	if err != nil {
		log.Printf("ERROR:%v", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Mesa atualizada com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteTable is the handler used to receive a delete request to delete a table
func (m *TableRepository) DeleteTable(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	tableId, _ := strconv.Atoi(id)

	err := m.TableDB.DeleteTable(tableId)
	if err != nil {
		log.Printf("ERROR:%v", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Mesa deletada com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}
