package handlers

import (
	"errors"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/clients"
	"strconv"

	"github.com/go-chi/chi"
)

// ClientRepo the repository used by the handlers
var ClientRepo *ClientRepository

// ClientRepository is the repository type
type ClientRepository struct {
	App      *config.Application
	ClientDB repository.ClientRepository
}

func NewClientRepo(a *config.Application, db *driver.DB) *ClientRepository {
	return &ClientRepository{
		App:      a,
		ClientDB: clients.NewPostgresRepo(db.SQL, a),
	}
}

func NewClientHandlers(r *ClientRepository) {
	ClientRepo = r
}

// ClientsPage displays the client page
func (m *ClientRepository) ClientsPage(w http.ResponseWriter, r *http.Request) {
	clients, err := m.ClientDB.GetAllClients()
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar clientes")
		return
	}

	data := make(map[string]interface{})
	data["clients"] = clients

	render.Template(w, r, "clients.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateClient creates a new client
func (m *ClientRepository) CreateClient(w http.ResponseWriter, r *http.Request) {
	var client models.Client

	err := helpers.ReadJSON(w, r, &client)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	// verify if the email is already registered
	_, err = m.ClientDB.GetClientByEmail(client.Email)
	if err == nil {
		helpers.BadRequest(w, r, errors.New("email já cadastrado"))
		return
	}

	phone := helpers.RemovePhoneMask(client.Phone)
	client.Phone = phone

	msg := client.ValidateFields(client)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}

	err = m.ClientDB.CreateClient(client)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Cliente cadastrado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusCreated, payload)
}

// UpdateClient updates a client
func (m *ClientRepository) UpdateClient(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	clientID, err := strconv.Atoi(id)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var client models.Client

	err = helpers.ReadJSON(w, r, &client)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	clientDB, err := m.ClientDB.GetClientByID(clientID)
	if err != nil {
		helpers.NotFound(w, r, errors.New("cliente não encontrado"))
		return
	}

	if client.Email != clientDB.Email {
		_, err = m.ClientDB.GetClientByEmail(client.Email)
		if err == nil {
			helpers.BadRequest(w, r, errors.New("email já cadastrado"))
			return
		}
		ok := helpers.VerifyEmail(client.Email)
		if !ok {
			helpers.BadRequest(w, r, errors.New("email inválido"))
			return
		}
	}

	if client.Name == "" {
		client.Name = clientDB.Name
	}
	if client.Phone == "" {
		client.Phone = clientDB.Phone
	}

	client.ID = clientID

	err = m.ClientDB.UpdateClient(client)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Cliente atualizado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteClient deletes a client
func (m *ClientRepository) DeleteClient(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	clientID, err := strconv.Atoi(id)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	err = m.ClientDB.DeleteClient(clientID)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Cliente deletado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}
