package handlers

import (
	"errors"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/category"
	"restaurante/internal/repository/dishes"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
)

// DishRepo the repository used by the handlers
var DishRepo *DishRepository

// DishRepository is the repository type
type DishRepository struct {
	App        *config.Application
	DishDB     repository.DishRepository
	CategoryDB repository.CategoryRepository
}

// NewDishRepo sets the repository for the handlers
func NewDishRepo(a *config.Application, db *driver.DB) *DishRepository {
	return &DishRepository{
		App:        a,
		DishDB:     dishes.NewPostgresRepo(db.SQL, a),
		CategoryDB: category.NewPostgresRepo(db.SQL, a),
	}
}

// NewDishHandlers sets the repository for the handlers
func NewDishHandlers(r *DishRepository) {
	DishRepo = r
}

// DishesPage displays the dishes page
func (m *DishRepository) DishesPage(w http.ResponseWriter, r *http.Request) {
	dishes, err := m.DishDB.GetAllDishes()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível carregar os pratos")
		return
	}

	categories, err := m.CategoryDB.GetAllCategories()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível carregar as categorias")
		return
	}

	data := make(map[string]interface{})
	data["categories"] = categories
	data["dishes"] = dishes

	render.Template(w, r, "dishes.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateDish creates a new dish
func (m *DishRepository) CreateDish(w http.ResponseWriter, r *http.Request) {
	var dish models.Dish

	err := helpers.ReadJSON(w, r, &dish)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, err)
		return
	}
	errorMsg := dish.ValidateFields(dish)
	if errorMsg != "" {
		m.App.ErrorLog.Println(errors.New(errorMsg))
		helpers.BadRequest(w, r, errors.New(errorMsg))
		return
	}

	// set the is_active field to true by default
	dish.IsActive = true
	err = m.DishDB.CreateDish(&dish)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Prato criado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdateDish updates a dish
func (m *DishRepository) UpdateDish(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	dishID, err := strconv.Atoi(id)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return
	}

	var inputDish struct {
		Name        string  `json:"name"`
		Description string  `json:"description"`
		Price       float64 `json:"price"`
	}

	err = helpers.ReadJSON(w, r, &inputDish)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, err)
		return
	}

	if inputDish.Name == "" {
		helpers.BadRequest(w, r, errors.New("o nome do prato não pode ser vazio"))
		return
	}
	if inputDish.Description == "" {
		helpers.BadRequest(w, r, errors.New("a descrição do prato não pode ser vazia"))
		return
	}
	if inputDish.Price == 0 || inputDish.Price < 0 {
		helpers.BadRequest(w, r, errors.New("o preço do prato não pode ser vazio"))
		return
	}

	inputDish.Description = strings.TrimSpace(inputDish.Description)

	dish := models.Dish{
		ID:          dishID,
		Name:        inputDish.Name,
		Description: inputDish.Description,
		Price:       inputDish.Price,
	}

	err = m.DishDB.UpdateDish(&dish)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Prato atualizado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteDish deletes a dish
func (m *DishRepository) DeleteDish(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	dishID, err := strconv.Atoi(id)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return
	}

	err = m.DishDB.DeleteDish(dishID)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Prato deletado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdateDishStatus updates the status of a dish
func (m *DishRepository) UpdateDishStatus(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	dishID, err := strconv.Atoi(id)
	if err != nil {
		m.App.ErrorLog.Println(err)
		return
	}

	var inputDish struct {
		IsActive bool `json:"is_active"`
	}

	err = helpers.ReadJSON(w, r, &inputDish)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, err)
		return
	}

	err = m.DishDB.UpdateDishStatus(dishID, inputDish.IsActive)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Status do prato atualizado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// ReportsDishesPage displays the reports dishes page
func (m *DishRepository) ReportsDishesPage(w http.ResponseWriter, r *http.Request) {
	dishes, err := m.DishDB.GetAllDishes()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível gerar o relatório de pratos")
		http.Redirect(w, r, "/dishes", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["dishes"] = dishes

	render.Template(w, r, "dishes-reports.page.tmpl", &models.TemplateData{
		Data: data,
	})
}
