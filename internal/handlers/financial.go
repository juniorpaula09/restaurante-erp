package handlers

import (
	"errors"
	"log"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/financial"
	"restaurante/internal/repository/product"
	"strconv"
	"time"

	"github.com/go-chi/chi"
)

var FinancialRepo *FinancialRepository

// FinancialRepository represents the financial repository
type FinancialRepository struct {
	App         *config.Application
	FinancialDB repository.FinancialRepository
	ProductDb   repository.ProductRepository
}

// NewFinancialRepo creates a new financial repository
func NewFinancialRepo(a *config.Application, db *driver.DB) *FinancialRepository {
	return &FinancialRepository{
		App:         a,
		FinancialDB: financial.NewPostgresRepo(db.SQL, a),
		ProductDb:   product.NewPostgresRepo(db.SQL, a),
	}
}

// NewFinancialHandlers sets the repository for the handlers
func NewFinancialHandlers(r *FinancialRepository) {
	FinancialRepo = r
}

// BillPayPage displays the financial page
func (m *FinancialRepository) BillPayPage(w http.ResponseWriter, r *http.Request) {
	billpays, err := m.FinancialDB.GetAllBillPays()
	if err != nil {
		log.Fatalf("Error getting all purchases: %s", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar contas a pagar")
		return
	}

	data := make(map[string]interface{})
	data["billpays"] = billpays

	render.Template(w, r, "bill-pay.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateBillPay creates a new bill payable
func (m *FinancialRepository) CreateBillPay(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Amount      float64 `json:"amount"`
		Description string  `json:"description"`
		DueDate     string  `json:"due_date"`
	}

	err := helpers.ReadJSON(w, r, &input)
	if err != nil {
		log.Printf("Error reading JSON: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	if input.Amount == 0 {
		helpers.BadRequest(w, r, errors.New("valor inválido"))
		return
	}

	// get userId from session
	userId := m.App.Session.GetInt(r.Context(), "user_id")

	purchase := models.PurchaseMoviment{
		Amount:       input.Amount,
		PurchaseDate: time.Now(),
		UserID:       userId,
		SupplierID:   0, // set as default
		Status:       "pendente",
		Type:         "saida",
	}

	// create purchase
	purchaseId, err := m.ProductDb.CreatePurchaseMoviments(&purchase)
	if err != nil {
		log.Printf("Error creating purchase: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	// create bill payable
	purchaseDueDate, err := time.Parse("2006-01-02", input.DueDate)
	if err != nil {
		log.Printf("Error parsing date: %s", err)
		helpers.ServerError(w, r, errors.New("data inválida"))
		return
	}

	billPay := models.BillPayable{
		Amount:             input.Amount,
		Description:        input.Description,
		UserID:             userId,
		PurchaseMovimentID: purchaseId,
		Status:             "pendente",
		DueDate:            purchaseDueDate,
		BillDate:           time.Now(),
	}

	err = m.ProductDb.CreateBillPayable(&billPay)
	if err != nil {
		log.Printf("Error creating bill payable: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	payload.Error = false
	payload.Message = "Conta a pagar criada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteBillPay deletes a bill payable
func (m *FinancialRepository) DeleteBillPay(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	billpayID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	bp, err := m.FinancialDB.GetBillPayByID(billpayID)
	if err != nil {
		log.Printf("Error getting bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}
	if bp.Status == "pago" {
		helpers.BadRequest(w, r, errors.New("esta Conta já foi paga, você não pode excluí-la"))
		return
	}
	if bp.Description == "Compra de produtos" {
		helpers.BadRequest(w, r, errors.New("esta conta foi lançada pelo Gerente / Administrador, você não pode excluí-la"))
		return
	}

	err = m.FinancialDB.DeleteBillPay(billpayID)
	if err != nil {
		log.Printf("Error deleting bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	err = m.FinancialDB.DeletePurchaseMovimentByID(bp.PurchaseMovimentID)
	if err != nil {
		log.Printf("Error deleting purchase moviment: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Conta excluída com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// BillPayOnePage displays the bill pay page
func (m *FinancialRepository) BillPayOnePage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	billpayID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar conta")
		http.Redirect(w, r, "/billpay", http.StatusSeeOther)
	}

	billpay, err := m.FinancialDB.GetBillPayByID(billpayID)
	if err != nil {
		log.Printf("Error getting bill pay: %s", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar conta")
		http.Redirect(w, r, "/billpay", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["billpay"] = billpay

	render.Template(w, r, "billpay-one.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// UpdateBillPay updates a bill payable
func (m *FinancialRepository) UpdateBillPay(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	billpayID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var input struct {
		Amount      float64 `json:"amount"`
		Description string  `json:"description"`
	}

	err = helpers.ReadJSON(w, r, &input)
	if err != nil {
		log.Printf("Error reading JSON: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	if input.Amount == 0 {
		helpers.BadRequest(w, r, errors.New("valor inválido"))
		return
	}

	userId := m.App.Session.GetInt(r.Context(), "user_id")

	billpay, err := m.FinancialDB.GetBillPayByID(billpayID)
	if err != nil {
		log.Printf("Error getting bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	billpay.UserID = userId
	billpay.Amount = input.Amount
	billpay.Description = input.Description

	err = m.FinancialDB.UpdateBillPay(billpay)
	if err != nil {
		log.Printf("Error updating bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Conta atualizada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// WriteOffBillPay writes off a bill payable
func (m *FinancialRepository) WriteOffBillPay(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	billpayID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	billpay, err := m.FinancialDB.GetBillPayByID(billpayID)
	if err != nil {
		log.Printf("Error getting bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	if billpay.Status == "pago" {
		helpers.BadRequest(w, r, errors.New("esta conta já foi paga"))
		return
	}

	userId := m.App.Session.GetInt(r.Context(), "user_id")

	billpay.UserID = userId
	billpay.Status = "pago"
	billpay.BillDate = time.Now()

	err = m.FinancialDB.UpdateBillPay(billpay)
	if err != nil {
		log.Printf("Error updating bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	pm, err := m.FinancialDB.GetPurchaseMovimentByID(billpay.PurchaseMovimentID)
	if err != nil {
		log.Printf("Error getting purchase moviment: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	pm.Status = "pago"
	pm.Type = "saida"
	pm.UserID = userId
	pm.UpdatedAt = time.Now()

	err = m.FinancialDB.UpdatePuchaseMoviment(pm)
	if err != nil {
		log.Printf("Error updating purchase moviment: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Conta baixada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// BillReceivePage displays the bill receive page
func (m *FinancialRepository) BillReceivePage(w http.ResponseWriter, r *http.Request) {
	br, err := m.FinancialDB.GetAllBillReceives()
	if err != nil {
		log.Printf("Error getting all bill receives: %s", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar contas a receber")
		return
	}

	data := make(map[string]interface{})
	data["billreceives"] = br

	render.Template(w, r, "bill-receive.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateBillReceive creates a new bill receive
func (m *FinancialRepository) CreateBillReceive(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Amount      float64 `json:"amount"`
		Description string  `json:"description"`
		DueDate     string  `json:"due_date"`
	}

	err := helpers.ReadJSON(w, r, &input)
	if err != nil {
		log.Printf("Error reading JSON: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	if input.Amount == 0 {
		helpers.BadRequest(w, r, errors.New("valor inválido"))
		return
	}

	// get userId from session
	userId := m.App.Session.GetInt(r.Context(), "user_id")

	purchase := models.PurchaseMoviment{
		Amount:       input.Amount,
		PurchaseDate: time.Now(),
		UserID:       userId,
		SupplierID:   0, // set as default
		Status:       "pendente",
		Type:         "entrada",
	}

	// create purchase
	purchaseId, err := m.ProductDb.CreatePurchaseMoviments(&purchase)
	if err != nil {
		log.Printf("Error creating purchase: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	// create bill receive
	dueDate, err := time.Parse("2006-01-02", input.DueDate)
	if err != nil {
		log.Printf("Error parsing date: %s", err)
		helpers.ServerError(w, r, errors.New("data inválida"))
		return
	}

	billReceive := models.BillReceive{
		Amount:             input.Amount,
		Description:        input.Description,
		UserID:             userId,
		PurchaseMovimentID: purchaseId,
		Status:             "pendente",
		DueDate:            dueDate,
		BillDate:           time.Now(),
	}

	err = m.FinancialDB.CreateBillReceive(billReceive)
	if err != nil {
		log.Printf("Error creating bill reecive: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	payload.Error = false
	payload.Message = "Conta à receber criada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteBillReceive deletes a bill receives
func (m *FinancialRepository) DeleteBillReceive(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	billID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	br, err := m.FinancialDB.GetBillReceiveByID(billID)
	if err != nil {
		log.Printf("Error getting bill receive: %s", err)
		helpers.ServerError(w, r, err)
		return
	}
	if br.Status == "pago" {
		helpers.BadRequest(w, r, errors.New("esta Conta já foi paga, você não pode excluí-la"))
		return
	}

	err = m.FinancialDB.DeleteBillReceive(billID)
	if err != nil {
		log.Printf("Error deleting bill receive: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	err = m.FinancialDB.DeletePurchaseMovimentByID(br.PurchaseMovimentID)
	if err != nil {
		log.Printf("Error deleting purchase moviment: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Conta excluída com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// BillReceiveOnePage displays the bill receive page
func (m *FinancialRepository) BillReceiveOnePage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	brID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar conta")
		http.Redirect(w, r, "/bill-receive", http.StatusSeeOther)
	}

	br, err := m.FinancialDB.GetBillReceiveByID(brID)
	if err != nil {
		log.Printf("Error getting bill receive: %s", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar conta")
		http.Redirect(w, r, "/bill-receive", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["billreceive"] = br

	render.Template(w, r, "bill-receive-one.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// UpdateBillReceive updates a bill receive
func (m *FinancialRepository) UpdateBillReceive(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	brID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var input struct {
		Amount      float64 `json:"amount"`
		Description string  `json:"description"`
	}

	err = helpers.ReadJSON(w, r, &input)
	if err != nil {
		log.Printf("Error reading JSON: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	if input.Amount == 0 {
		helpers.BadRequest(w, r, errors.New("valor inválido"))
		return
	}

	userId := m.App.Session.GetInt(r.Context(), "user_id")

	billreceive, err := m.FinancialDB.GetBillReceiveByID(brID)
	if err != nil {
		log.Printf("Error getting bill receive: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	billreceive.UserID = userId
	billreceive.Amount = input.Amount
	billreceive.Description = input.Description

	err = m.FinancialDB.UpdateBillReceive(billreceive)
	if err != nil {
		log.Printf("Error updating bill receive: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Conta atualizada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// WriteOffBillReceive writes off a bill receive
func (m *FinancialRepository) WriteOffBillReceive(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	brID, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("Error getting url param: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	billreceive, err := m.FinancialDB.GetBillReceiveByID(brID)
	if err != nil {
		log.Printf("Error getting bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	if billreceive.Status == "pago" {
		helpers.BadRequest(w, r, errors.New("esta conta já foi paga"))
		return
	}

	userId := m.App.Session.GetInt(r.Context(), "user_id")

	billreceive.UserID = userId
	billreceive.Status = "pago"
	billreceive.BillDate = time.Now()

	err = m.FinancialDB.UpdateBillReceive(billreceive)
	if err != nil {
		log.Printf("Error updating bill pay: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	pm, err := m.FinancialDB.GetPurchaseMovimentByID(billreceive.PurchaseMovimentID)
	if err != nil {
		log.Printf("Error getting purchase moviment: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	pm.Status = "pago"
	pm.Type = "entrada"
	pm.UserID = userId
	pm.UpdatedAt = time.Now()

	err = m.FinancialDB.UpdatePuchaseMoviment(pm)
	if err != nil {
		log.Printf("Error updating purchase moviment: %s", err)
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Conta baixada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// PurchaseMovimentPage displays the purchase moviment page
func (m *FinancialRepository) PurchaseMovimentPage(w http.ResponseWriter, r *http.Request) {
	pm, err := m.FinancialDB.GetAllPurchaseMoviments()
	if err != nil {
		log.Printf("Error getting all purchase moviments: %s", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar movimentações")
		return
	}

	data := make(map[string]interface{})
	data["moviments"] = pm

	render.Template(w, r, "financial-moviments.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// ReportsBillPaysPage displays the reports bill pays page
func (m *FinancialRepository) ReportsBillPaysPage(w http.ResponseWriter, r *http.Request) {
	startDate := r.URL.Query().Get("billpay_start_date")
	endDate := r.URL.Query().Get("billpay_end_date")
	status := r.URL.Query().Get("billpay_report_status")

	if status == "0" {
		status = ""
	} else if status == "1" {
		status = "pago"
	} else if status == "2" {
		status = "pendente"
	}

	billpay, err := m.FinancialDB.GetAllBillpaysFromReport(startDate, endDate, status)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting billpays: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de contas à pagar")
		http.Redirect(w, r, "/sistema/billpay", http.StatusSeeOther)
	}

	total := 0.0
	for _, b := range billpay {
		total += b.Amount
	}

	sDate, err := time.Parse("2006-01-02", startDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de contas à pagar")
		http.Redirect(w, r, "/sistema/billpay", http.StatusSeeOther)
	}
	eDate, err := time.Parse("2006-01-02", endDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de contas à pagar")
		http.Redirect(w, r, "/sistema/billpay", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["billpay"] = billpay
	data["start_date"] = sDate
	data["end_date"] = eDate
	data["total"] = total

	render.Template(w, r, "billpay-reports.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// ReportsBillReceivesPage displays the reports bill receives page
func (m *FinancialRepository) ReportsBillReceivesPage(w http.ResponseWriter, r *http.Request) {
	startDate := r.URL.Query().Get("billreceive_start_date")
	endDate := r.URL.Query().Get("billreceive_end_date")
	status := r.URL.Query().Get("billreceive_report_status")

	if status == "0" {
		status = ""
	} else if status == "1" {
		status = "pago"
	} else if status == "2" {
		status = "pendente"
	}

	billreceive, err := m.FinancialDB.GetAllBillReceiveFromReport(startDate, endDate, status)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting billreceive: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de contas à receber")
		http.Redirect(w, r, "/sistema/bill-receive", http.StatusSeeOther)
	}

	total := 0.0
	for _, b := range billreceive {
		total += b.Amount
	}

	sDate, err := time.Parse("2006-01-02", startDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de contas à receber")
		http.Redirect(w, r, "/sistema/bill-receive", http.StatusSeeOther)
	}
	eDate, err := time.Parse("2006-01-02", endDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de contas à receber")
		http.Redirect(w, r, "/sistema/bill-receive", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["billreceive"] = billreceive
	data["start_date"] = sDate
	data["end_date"] = eDate
	data["total"] = total

	render.Template(w, r, "bill-receive-reports.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// ReportsMovimentsPage displays the reports moviments page
func (m *FinancialRepository) ReportsMovimentsPage(w http.ResponseWriter, r *http.Request) {
	startDate := r.URL.Query().Get("moviments_start_date")
	endDate := r.URL.Query().Get("moviments_end_date")
	mType := r.URL.Query().Get("moviments_report_status")

	if mType == "0" {
		mType = ""
	} else if mType == "1" {
		mType = "entrada"
	} else if mType == "2" {
		mType = "saida"
	}

	mv, err := m.FinancialDB.GetAllMovimentsFromReport(startDate, endDate, mType)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting moviments: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de Movimentações")
		http.Redirect(w, r, "/sistema/moviments", http.StatusSeeOther)
	}

	total := 0.0
	for _, m := range mv {
		total += m.Amount
	}

	sDate, err := time.Parse("2006-01-02", startDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de Movimentações")
		http.Redirect(w, r, "/sistema/moviments", http.StatusSeeOther)
	}
	eDate, err := time.Parse("2006-01-02", endDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de Movimentações")
		http.Redirect(w, r, "/sistema/moviments", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["moviments"] = mv
	data["start_date"] = sDate
	data["end_date"] = eDate
	data["total"] = total

	render.Template(w, r, "financial-moviments-reports.page.tmpl", &models.TemplateData{
		Data: data,
	})
}
