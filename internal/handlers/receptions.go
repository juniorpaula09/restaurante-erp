package handlers

import (
	"errors"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/clients"
	"restaurante/internal/repository/reception"
	"restaurante/internal/repository/tables"
	"restaurante/internal/repository/users"
	"strconv"
	"time"

	"github.com/go-chi/chi"
)

// ReceptionRepo the repository used by the handlers
var ReceptionRepo *ReceptionRepository

// ReceptionRepository is the repository type
type ReceptionRepository struct {
	App           *config.Application
	ReceptionRepo repository.ReceptionRepository
	TablesDB      repository.TableRepository
	ClientDB      repository.ClientRepository
	UserRepo      repository.UserRepository
}

// NewReceptionRepo sets the repository for the handlers
func NewReceptionRepo(a *config.Application, db *driver.DB) *ReceptionRepository {
	return &ReceptionRepository{
		App:           a,
		ReceptionRepo: reception.NewPostgresRepo(db.SQL, a),
		TablesDB:      tables.NewPostgresRepo(db.SQL, a),
		ClientDB:      clients.NewPostgresRepo(db.SQL, a),
		UserRepo:      users.NewPostgresRepo(db.SQL, a),
	}
}

// NewUserHandlers sets the repository for the handlers
func NewReceptionHandlers(r *ReceptionRepository) {
	ReceptionRepo = r
}

// ReservationsPage displays the reception page
func (m *ReceptionRepository) ReservationsPage(w http.ResponseWriter, r *http.Request) {
	clients, err := m.ClientDB.GetAllClients()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar clientes")
		return
	}

	dataNow := time.Now()

	data := make(map[string]interface{})
	data["date_now"] = dataNow.Format("2006-01-02")
	data["clients"] = clients

	render.Template(w, r, "reservations.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// ReservationsTables return the all tables for reversation page via api calls
func (m *ReceptionRepository) ReservationsTables(w http.ResponseWriter, r *http.Request) {
	date := r.URL.Query().Get("date")

	tables, err := m.TablesDB.AllTables()
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	// get reservation by tableid and date
	for _, table := range tables {
		reservations, err := m.ReceptionRepo.GetReservationsByTableIDAndDate(table.ID, date)
		if err != nil {
			m.App.ErrorLog.Println(err)
			helpers.ServerError(w, r, errors.New("internal server error"))
			return
		}
		table.Reservations = reservations

		// get client by id
		for _, res := range table.Reservations {
			client, err := m.ClientDB.GetClientByID(res.ClientID)
			if err != nil {
				m.App.ErrorLog.Println(err)
				helpers.ServerError(w, r, errors.New("internal server error"))
				return
			}
			res.Client = &client
		}
	}

	var payload struct {
		Error  bool            `json:"error"`
		Tables []*models.Table `json:"tables"`
	}
	payload.Error = false
	payload.Tables = tables

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// AllReservations render the all reservations page
func (m *ReceptionRepository) AllReservations(w http.ResponseWriter, r *http.Request) {
	reservations, err := m.ReceptionRepo.GetAllReservations()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar reservas")
		return
	}

	for _, res := range reservations {
		client, err := m.ClientDB.GetClientByID(res.ClientID)
		if err != nil {
			m.App.ErrorLog.Println(err)
			m.App.Session.Put(r.Context(), "error", "Erro ao buscar cliente")
			return
		}
		res.Client = &client

		table, err := m.TablesDB.GetTableByID(res.TableID)
		if err != nil {
			m.App.ErrorLog.Println(err)
			m.App.Session.Put(r.Context(), "error", "Erro ao buscar mesa")
			return
		}
		res.Table = table

		if res.EmployeeID != 0 {
			employee, err := m.UserRepo.GetUserByID(res.EmployeeID)
			if err != nil {
				m.App.ErrorLog.Println(err)
				m.App.Session.Put(r.Context(), "error", "Erro ao buscar funcionário")
				return
			}
			res.Employee = &employee
		}

		res.Date = res.ConvertDate()
	}

	data := make(map[string]interface{})
	data["reservations"] = reservations

	render.Template(w, r, "all-reservations.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// MakeReservation make a reservation
func (m *ReceptionRepository) MakeReservation(w http.ResponseWriter, r *http.Request) {
	var inputData models.Reservation

	err := helpers.ReadJSON(w, r, &inputData)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	userId := m.App.Session.GetInt(r.Context(), "user_id")
	inputData.EmployeeID = userId

	err = m.ReceptionRepo.CreateReservation(&inputData)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Reserva realizada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteReservation delete a reservation
func (m *ReceptionRepository) DeleteReservation(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("table_id")
	tableId, err := strconv.Atoi(id)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	err = m.ReceptionRepo.DeleteReservationByTableID(tableId)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Reserva cancelada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteReservationByID delete a reservation by id
func (m *ReceptionRepository) DeleteReservationByID(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	reservationId, err := strconv.Atoi(id)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	err = m.ReceptionRepo.DeleteReservationByID(reservationId)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Reserva cancelada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// CountReservationsByDate return the number of reservations by date
func (m *ReceptionRepository) CountReservationsByDate(w http.ResponseWriter, r *http.Request) {
	start := r.URL.Query().Get("start")
	end := r.URL.Query().Get("end")

	count, err := m.ReceptionRepo.CountReservationByDate(start, end)
	if err != nil {
		m.App.ErrorLog.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error     bool   `json:"error"`
		Message   string `json:"message"`
		Count     int    `json:"count"`
		StartDate string `json:"start_date"`
	}
	payload.Error = false
	payload.Message = "Reservas encontradas"
	payload.Count = count
	payload.StartDate = start

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}
