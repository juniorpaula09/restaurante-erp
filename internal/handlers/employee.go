package handlers

import (
	"errors"
	"log"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/employeers"
	"strconv"

	"github.com/go-chi/chi"
)

// EmployeeRepo the repository used by the handlers
var EmployeeRepo *EmployeeRepository

// EmployeeRepository is the repository type
type EmployeeRepository struct {
	App        *config.Application
	EmployeeDB repository.EmployeeRepository
}

func NewEmployeeRepo(a *config.Application, db *driver.DB) *EmployeeRepository {
	return &EmployeeRepository{
		App:        a,
		EmployeeDB: employeers.NewPostgresRepo(db.SQL, a),
	}
}

// NewEmployeeHandlers sets the repository for the handlers
func NewEmployeeHandlers(r *EmployeeRepository) {
	EmployeeRepo = r
}

// EmployeesPage is the handler used to render the employees page
func (m *EmployeeRepository) EmployeesPage(w http.ResponseWriter, r *http.Request) {
	positions, err := m.EmployeeDB.GetPositions()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível obter os cargos.")
		return
	}

	data := make(map[string]interface{})
	data["positions"] = positions

	employeers, err := m.EmployeeDB.GetAllEmployees()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível obter os funcionários.")
		return
	}
	data["employees"] = employeers

	render.Template(w, r, "employee.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// EmployeePage is the handler used to render the employee page
func (m *EmployeeRepository) EmployeePage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	employeeID, err := strconv.Atoi(id)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	employee, err := m.EmployeeDB.GetEmployeeByID(employeeID)
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível obter o funcionário.")
		return
	}

	positions, err := m.EmployeeDB.GetPositions()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível obter os cargos.")
		return
	}

	data := make(map[string]interface{})
	data["employee"] = employee
	data["positions"] = positions

	render.Template(w, r, "employee-edit.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateEmployee is the handler used to create a new employee
func (m *EmployeeRepository) CreateEmployee(w http.ResponseWriter, r *http.Request) {
	var inputEmployee struct {
		FirstName   string `json:"first_name"`
		LastName    string `json:"last_name"`
		Email       string `json:"email"`
		CPF         string `json:"cpf"`
		Phone       string `json:"phone"`
		Address     string `json:"address"`
		AccessLevel int    `json:"access_level"`
	}

	err := helpers.ReadJSON(w, r, &inputEmployee)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	// remove the mask from the cpf
	cpf := helpers.RemoveCPFMask(inputEmployee.CPF)
	inputEmployee.CPF = cpf

	// remove the mask from the phone
	phone := helpers.RemovePhoneMask(inputEmployee.Phone)
	inputEmployee.Phone = phone

	var employee models.Employee
	msg := employee.ValidateFields(inputEmployee)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}

	employee = models.Employee{
		FirstName:   inputEmployee.FirstName,
		LastName:    inputEmployee.LastName,
		Email:       inputEmployee.Email,
		CPF:         inputEmployee.CPF,
		Phone:       inputEmployee.Phone,
		Address:     inputEmployee.Address,
		AccessLevel: inputEmployee.AccessLevel,
	}

	err = m.EmployeeDB.CreateEmployee(&employee)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Funcionário cadastrado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdateEmployee is the handler used to update an employee
func (m *EmployeeRepository) UpdateEmployee(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	employeeID, err := strconv.Atoi(id)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var inputEmployee struct {
		FirstName   string `json:"first_name"`
		LastName    string `json:"last_name"`
		Email       string `json:"email"`
		CPF         string `json:"cpf"`
		Phone       string `json:"phone"`
		Address     string `json:"address"`
		AccessLevel int    `json:"access_level"`
	}

	err = helpers.ReadJSON(w, r, &inputEmployee)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	// remove the mask from the cpf
	cpf := helpers.RemoveCPFMask(inputEmployee.CPF)
	inputEmployee.CPF = cpf

	// remove the mask from the phone
	phone := helpers.RemovePhoneMask(inputEmployee.Phone)
	inputEmployee.Phone = phone

	var employee models.Employee
	msg := employee.ValidateFields(inputEmployee)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}

	employee = models.Employee{
		ID:          employeeID,
		FirstName:   inputEmployee.FirstName,
		LastName:    inputEmployee.LastName,
		Email:       inputEmployee.Email,
		CPF:         inputEmployee.CPF,
		Phone:       inputEmployee.Phone,
		Address:     inputEmployee.Address,
		AccessLevel: inputEmployee.AccessLevel,
	}

	err = m.EmployeeDB.UpdateEmployee(&employee)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Funcionário atualizado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteEmployee is the handler used to delete an employee
func (m *EmployeeRepository) DeleteEmployee(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	employeeID, err := strconv.Atoi(id)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	err = m.EmployeeDB.DeleteEmployee(employeeID)
	if err != nil {
		log.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Funcionário deletado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// PositionsPage is the handler used to render the positions page
func (m *EmployeeRepository) PositionsPage(w http.ResponseWriter, r *http.Request) {
	pos, err := m.EmployeeDB.GetPositions()
	if err != nil {
		m.App.ErrorLog.Println(err)
		m.App.Session.Put(r.Context(), "error", "Não foi possível obter os cargos.")
		return
	}

	data := make(map[string]interface{})
	data["positions"] = pos

	render.Template(w, r, "positions.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreatePosition is the handler used to create a new position
func (m *EmployeeRepository) CreatePosition(w http.ResponseWriter, r *http.Request) {
	var inputPosition struct {
		Name string `json:"position_name"`
	}

	err := helpers.ReadJSON(w, r, &inputPosition)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	accessLevel := render.ConvertPositionToAccessLevel(inputPosition.Name)

	position := &models.Position{
		Name:        inputPosition.Name,
		AccessLevel: accessLevel,
	}

	err = m.EmployeeDB.CreatePosition(position)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdatePosition is the handler used to update a position
func (m *EmployeeRepository) UpdatePosition(w http.ResponseWriter, r *http.Request) {
	var inputPosition struct {
		ID   int    `json:"id"`
		Name string `json:"name"`
	}

	err := helpers.ReadJSON(w, r, &inputPosition)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	accessLevel := render.ConvertPositionToAccessLevel(inputPosition.Name)

	var position = &models.Position{
		ID:          inputPosition.ID,
		Name:        inputPosition.Name,
		AccessLevel: accessLevel,
	}

	err = m.EmployeeDB.UpdatePosition(position)
	if err != nil {
		log.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Cargo atualizado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeletePosition is the handler used to delete a position
func (m *EmployeeRepository) DeletePosition(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	posID, err := strconv.Atoi(id)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	err = m.EmployeeDB.DeletePosition(posID)
	if err != nil {
		log.Println(err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Cargo deletado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}
