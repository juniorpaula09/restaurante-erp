package handlers

import (
	"errors"
	"log"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/supplier"
	"strconv"

	"github.com/go-chi/chi"
)

// SupplierRepo the repository used by the handlers
var SupplierRepo *SupplierRepository

// NewSupplierRepo sets the repository for the handlers
type SupplierRepository struct {
	App        *config.Application
	SupplierDB repository.SupplierRepository
}

// NewSupplierRepo sets the repository for the handlers
func NewSupplierRepo(a *config.Application, db *driver.DB) *SupplierRepository {
	return &SupplierRepository{
		App:        a,
		SupplierDB: supplier.NewPostgresRepo(db.SQL, a),
	}
}

// NewSupplierHandlers sets the repository for the handlers
func NewSupplierHandlers(r *SupplierRepository) {
	SupplierRepo = r
}

// SuppliersPage is the handler used to render the suppliers page
func (m *SupplierRepository) SuppliersPage(w http.ResponseWriter, r *http.Request) {
	suppliers, err := m.SupplierDB.GetAll()
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar fornecedores")
		return
	}

	var suppliersAux []models.Supplier

	idxToRemove := 0
	for _, s := range suppliers {
		if s.ID != idxToRemove {
			suppliersAux = append(suppliersAux, *s)
		}
	}

	data := make(map[string]interface{})
	data["suppliers"] = suppliersAux

	render.Template(w, r, "suppliers.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// SupplierPage is the handler used to render the supplier page
func (m *SupplierRepository) SupplierPage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	supId, _ := strconv.Atoi(id)

	sup, err := m.SupplierDB.GetSupplierByID(supId)
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar fornecedor")
		return
	}

	data := make(map[string]interface{})
	data["suppliers"] = sup

	render.Template(w, r, "supplier-edit.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateSupplier is the handler used to create a new supplier
func (m *SupplierRepository) CreateSupplier(w http.ResponseWriter, r *http.Request) {
	var inputSupplier models.Supplier

	err := helpers.ReadJSON(w, r, &inputSupplier)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal serve error"))
		return
	}

	msg := inputSupplier.ValidateFields(inputSupplier)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}

	sup, err := m.SupplierDB.GetSupplierByEmail(inputSupplier.Email)
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	if sup != nil {
		helpers.BadRequest(w, r, errors.New("email já existe"))
		return
	}

	phone := helpers.RemovePhoneMask(inputSupplier.Phone)
	inputSupplier.Phone = phone

	err = m.SupplierDB.CreateSupplier(&inputSupplier)
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Fornecedor adicionado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdateSupplier is the handler used to update a supplier
func (m *SupplierRepository) UpdateSupplier(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	supId, _ := strconv.Atoi(id)

	var inputSupplier models.Supplier

	err := helpers.ReadJSON(w, r, &inputSupplier)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal serve error"))
		return
	}

	sup, err := m.SupplierDB.GetSupplierByID(supId)
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		helpers.NotFound(w, r, errors.New("fornecedor não encontrado"))
		return
	}

	if inputSupplier.Email != sup.Email {
		s, err := m.SupplierDB.GetSupplierByEmail(inputSupplier.Email)
		if err != nil {
			helpers.ServerError(w, r, errors.New("internal server error"))
			return
		}
		if s != nil {
			helpers.BadRequest(w, r, errors.New("email já cadastrado"))
			return
		}

		ok := helpers.VerifyEmail(inputSupplier.Email)
		if !ok {
			helpers.BadRequest(w, r, errors.New("email inválido"))
			return
		}
	}

	if inputSupplier.Name == "" {
		inputSupplier.Name = sup.Name
	}
	if inputSupplier.ProductType == "" {
		inputSupplier.ProductType = sup.ProductType
	}

	phone := helpers.RemovePhoneMask(inputSupplier.Phone)
	inputSupplier.Phone = phone

	err = m.SupplierDB.UpdateSupplier(supId, &inputSupplier)
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Fornecedor atualizado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteSupplier is the handler used to delete a supplier
func (m *SupplierRepository) DeleteSupplier(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	supId, _ := strconv.Atoi(id)

	err := m.SupplierDB.DeleteSupplier(supId)
	if err != nil {
		log.Printf("ERROR: %v\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Fornecedor deletado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}
