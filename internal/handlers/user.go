package handlers

import (
	"errors"
	"log"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/users"
	"strconv"

	"github.com/go-chi/chi"
)

// UserRepo the repository used by the handlers
var UserRepo *UserRepository

// UserRepository is the repository type
type UserRepository struct {
	App    *config.Application
	UserDB repository.UserRepository
}

func NewUserRepo(a *config.Application, db *driver.DB) *UserRepository {
	return &UserRepository{
		App:    a,
		UserDB: users.NewPostgresRepo(db.SQL, a),
	}
}

// NewUserHandlers sets the repository for the handlers
func NewUserHandlers(r *UserRepository) {
	UserRepo = r
}

// UsersPage is the handler used to render the users page
func (m *UserRepository) UsersPage(w http.ResponseWriter, r *http.Request) {
	users, err := m.UserDB.GetAllUsers()
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar usuários")
		return
	}

	data := make(map[string]interface{})
	data["users"] = users

	roleNumbers := make(map[int]int)
	roleNumbers[1] = 1
	roleNumbers[2] = 2
	roleNumbers[3] = 3
	roleNumbers[4] = 4
	roleNumbers[5] = 5
	data["roleNumbers"] = roleNumbers

	render.Template(w, r, "users.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// UserEditPage is the handler used to render the user edit page
func (m *UserRepository) UserEditPage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	userID, _ := strconv.Atoi(id)

	user, err := m.UserDB.GetUserByID(userID)
	if err != nil {
		m.App.Session.Put(r.Context(), "error", "Usuário não encontrado")
		return
	}

	data := make(map[string]interface{})
	data["user"] = user

	roleNumbers := make(map[int]int)
	roleNumbers[1] = 1
	roleNumbers[2] = 2
	roleNumbers[3] = 3
	roleNumbers[4] = 4
	roleNumbers[5] = 5
	data["roleNumbers"] = roleNumbers

	render.Template(w, r, "user-edit.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateUser is the handler used to create a new system user
func (m *UserRepository) CreateUser(w http.ResponseWriter, r *http.Request) {
	var userInput models.User

	err := helpers.ReadJSON(w, r, &userInput)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	msg := userInput.ValidateFields(userInput)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}

	_, err = m.UserDB.GetUserByEmail(userInput.Email)
	if err == nil {
		helpers.BadRequest(w, r, errors.New("email já cadastrado"))
		return
	}

	passwordHashed, err := helpers.HashPassword(userInput.Password)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}
	userInput.Password = passwordHashed

	err = m.UserDB.CreateUser(userInput)
	if err != nil {
		log.Println(err)
		helpers.ServerError(w, r, errors.New("erro ao criar o usuário"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Usuário criado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)

}

// EditOneUser is the handler used to edit one user
func (m *UserRepository) EditOneUser(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	userID, _ := strconv.Atoi(id)

	var userInput models.User

	err := helpers.ReadJSON(w, r, &userInput)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	user, err := m.UserDB.GetUserByID(userID)
	if err != nil {
		helpers.NotFound(w, r, errors.New("usuário não encontrado"))
		return
	}

	if userInput.Email != user.Email {
		_, err := m.UserDB.GetUserByEmail(userInput.Email)
		if err == nil {
			helpers.BadRequest(w, r, errors.New("email já cadastrado"))
			return
		}
		ok := helpers.VerifyEmail(userInput.Email)
		if !ok {
			helpers.BadRequest(w, r, errors.New("email inválido"))
			return
		}
	}

	if userInput.Password != "" {
		passwordHashed, err := helpers.HashPassword(userInput.Password)
		if err != nil {
			helpers.ServerError(w, r, errors.New("internal server error"))
			return
		}
		userInput.Password = passwordHashed
	} else {
		userInput.Password = user.Password
	}

	if userInput.AccessLevel == user.AccessLevel {
		userInput.AccessLevel = user.AccessLevel
	}

	if userInput.FirstName == "" {
		userInput.FirstName = user.FirstName
	}
	if userInput.LastName == "" {
		userInput.LastName = user.LastName
	}

	err = m.UserDB.UpdateOneUser(userID, userInput)
	if err != nil {
		helpers.ServerError(w, r, errors.New("erro ao atualizar o usuário"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Usuário atualizado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// EditUser is the handler used to user edit your own data
func (m *UserRepository) EditUser(w http.ResponseWriter, r *http.Request) {
	userID := m.App.Session.GetInt(r.Context(), "user_id")

	var userInput struct {
		FirstName string `json:"name"`
		LastName  string `json:"lastname"`
		Email     string `json:"email"`
	}

	err := helpers.ReadJSON(w, r, &userInput)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	u, err := m.UserDB.GetUserByID(userID)
	if err != nil {
		helpers.NotFound(w, r, errors.New("usuário não encontrado"))
		return
	}

	// verificar se o email já existe
	if userInput.Email != u.Email {
		_, err := m.UserDB.GetUserByEmail(userInput.Email)
		if err == nil {
			helpers.BadRequest(w, r, errors.New("email já cadastrado"))
			return
		}
	}

	if userInput.FirstName == "" {
		userInput.FirstName = u.FirstName
	}
	if userInput.LastName == "" {
		userInput.LastName = u.LastName
	}
	if userInput.Email == "" {
		userInput.Email = u.Email
	}

	var user models.User
	user.FirstName = userInput.FirstName
	user.LastName = userInput.LastName
	user.Email = userInput.Email

	err = m.UserDB.UpdateUser(userID, user)
	if err != nil {
		helpers.ServerError(w, r, errors.New("erro ao atualizar o usuário"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}

	payload.Error = false
	payload.Message = "Usuário atualizado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteUser is the handler used to delete a user
func (m *UserRepository) DeleteUser(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	userID, _ := strconv.Atoi(id)

	err := m.UserDB.DeleteUser(userID)
	if err != nil {
		helpers.ServerError(w, r, errors.New("erro ao deletar o usuário"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Usuário deletado com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}
