package handlers

import (
	"log"
	"net/http"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"time"
)

func (m *PageRepository) HomePage(w http.ResponseWriter, r *http.Request) {
	userID := m.App.Session.Get(r.Context(), "user_id")
	user, err := m.UserDB.GetUserByID(userID.(int))
	if err != nil {
		log.Println("Error ao busca o usuário", err)
	}

	supliers, err := m.SuplierDB.GetAll()
	if err != nil {
		log.Println("Error ao busca os fornecedores", err)
	}

	moviments, err := m.handlerMoviments()
	if err != nil {
		log.Println("Error ao busca os movimentos", err)
	}

	// get first day of month
	currYear, currMonth, _ := time.Now().Date()
	beginMonthDate := time.Date(currYear, currMonth, 1, 0, 0, 0, 0, time.UTC)

	billpays, err := m.handlerBillPay(beginMonthDate)
	if err != nil {
		log.Println("Error ao busca as contas a pagar", err)
	}

	billreceives, err := m.handlerBillReceive(beginMonthDate)
	if err != nil {
		log.Println("Error ao busca as contas a receber", err)
	}

	amountBalanceThisMont, err := m.handlerPurchaseMoviments(beginMonthDate)
	if err != nil {
		log.Println("Error ao busca as movimentações de compras", err)
	}

	quantityReservationsThisMont, err := m.handlerReservations(beginMonthDate)
	if err != nil {
		log.Println("Error ao busca as reservas", err)
	}

	data := make(map[string]interface{})
	data["user"] = user
	data["supliers"] = supliers
	data["income"] = moviments.Income
	data["outcome"] = moviments.Outcome
	data["balance"] = moviments.Balance
	data["classBalance"] = moviments.classBalance
	data["lastMoviment"] = moviments.lastMoviment
	data["billOverdues"] = billpays.OverdueBillPay
	data["billspayToday"] = billpays.BillPayToday
	data["amountToPayThisMont"] = billpays.amountToPayThisMont
	data["quantyBillPayThisMont"] = billpays.quantyBillPayThisMont
	data["overdueBillReceives"] = billreceives.OverdueBillReceive
	data["billreceivesToday"] = billreceives.BillReceiveToday
	data["amountToReceiveThisMont"] = billreceives.amountToReceiveThisMont
	data["quantyBillReceiveThisMont"] = billreceives.quantyBillReceiveThisMont
	data["amountBalanceThisMont"] = amountBalanceThisMont
	data["quantityReservationsThisMont"] = quantityReservationsThisMont

	render.Template(w, r, "home.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

type moviments struct {
	Income       float64
	Outcome      float64
	Balance      float64
	classBalance string
	lastMoviment models.PurchaseMoviment
}

func (m *PageRepository) handlerMoviments() (moviments, error) {
	mvs := moviments{}
	currDate := time.Now().Format("2006-01-02")
	moviments, err := m.FinancialDB.GetAllMovimentsByDate(currDate)
	if err != nil {
		return mvs, err
	}

	var classBalance string
	var lastMoviment models.PurchaseMoviment
	var income, outcome, balance float64
	if len(moviments) > 0 {
		for _, mov := range moviments {
			if mov.Type == "entrada" && mov.Status == "pago" {
				income += mov.Amount
			} else if mov.Type == "saida" && mov.Status == "pago" {
				outcome += mov.Amount
			}

			balance = income - outcome
			if balance <= 0 {
				classBalance = "balance-negative"
			} else {
				classBalance = "balance-positive"
			}

		}

		lastMoviment = moviments[len(moviments)-1]
	} else {
		classBalance = "balance-negative"
	}

	mvs.Income = income
	mvs.Outcome = outcome
	mvs.Balance = balance
	mvs.classBalance = classBalance
	mvs.lastMoviment = lastMoviment

	return mvs, nil
}

type billPay struct {
	OverdueBillPay        []models.BillPayable
	BillPayToday          []models.BillPayable
	amountToPayThisMont   float64
	quantyBillPayThisMont int
}

func (m *PageRepository) handlerBillPay(beginMonthDate time.Time) (billPay, error) {
	bp := billPay{}

	billpays, err := m.FinancialDB.GetAllBillPays()
	if err != nil {
		return bp, err
	}

	billOverdues := make([]models.BillPayable, 0)
	billspayToday := make([]models.BillPayable, 0)
	amountToPayThisMont := 0.0
	quantyBillPayThisMont := 0

	for _, bill := range billpays {
		if bill.DueDate.Before(time.Now()) && bill.Status == "pendente" {
			billOverdues = append(billOverdues, bill)
		}
		currentDate := bill.DueDate.Format("2006-01-02") == time.Now().Format("2006-01-02")
		if currentDate && bill.Status == "pendente" {
			billspayToday = append(billspayToday, bill)
		}
		if bill.DueDate.After(beginMonthDate) && bill.BillDate.Before(time.Now()) && bill.Status == "pendente" {
			amountToPayThisMont += bill.Amount
			quantyBillPayThisMont++
		}
	}

	bp.OverdueBillPay = billOverdues
	bp.BillPayToday = billspayToday
	bp.amountToPayThisMont = amountToPayThisMont
	bp.quantyBillPayThisMont = quantyBillPayThisMont

	return bp, nil
}

type billReceive struct {
	OverdueBillReceive        []models.BillReceive
	BillReceiveToday          []models.BillReceive
	amountToReceiveThisMont   float64
	quantyBillReceiveThisMont int
}

func (m *PageRepository) handlerBillReceive(beginMonthDate time.Time) (billReceive, error) {
	br := billReceive{}

	billreceives, err := m.FinancialDB.GetAllBillReceives()
	if err != nil {
		return br, err
	}

	overdueBillReceives := make([]models.BillReceive, 0)
	billreceivesToday := make([]models.BillReceive, 0)
	amountToReceiveThisMont := 0.0
	quantyBillReceiveThisMont := 0

	for _, bill := range billreceives {
		if bill.DueDate.Before(time.Now()) && bill.Status == "pendente" {
			overdueBillReceives = append(overdueBillReceives, bill)
		}
		currDate := bill.DueDate.Format("2006-01-02") == time.Now().Format("2006-01-02")
		if currDate && bill.Status == "pendente" {
			billreceivesToday = append(billreceivesToday, bill)
		}
		if bill.DueDate.After(beginMonthDate) && bill.BillDate.Before(time.Now()) && bill.Status == "pendente" {
			amountToReceiveThisMont += bill.Amount
			quantyBillReceiveThisMont++
		}
	}

	br.OverdueBillReceive = overdueBillReceives
	br.BillReceiveToday = billreceivesToday
	br.amountToReceiveThisMont = amountToReceiveThisMont
	br.quantyBillReceiveThisMont = quantyBillReceiveThisMont

	return br, nil
}

func (m *PageRepository) handlerPurchaseMoviments(beginMonthDate time.Time) (float64, error) {
	movs, err := m.FinancialDB.GetAllPurchaseMoviments()
	if err != nil {
		return 0, err
	}

	amountBalanceThisMont := 0.0

	for _, mov := range movs {
		if mov.PurchaseDate.After(beginMonthDate) && mov.PurchaseDate.Before(time.Now()) && mov.Type == "entrada" {
			amountBalanceThisMont += mov.Amount
		}
	}
	return amountBalanceThisMont, nil
}

func (m *PageRepository) handlerReservations(beginMonthDate time.Time) (int, error) {
	reservations, err := m.ReceptionDB.GetAllReservations()
	if err != nil {
		return 0, err
	}

	quantityReservationsThisMont := 0
	for _, res := range reservations {
		// convet string date to time
		date, _ := time.Parse("2006-01-02", res.Date)
		if date.After(beginMonthDate) && date.Before(beginMonthDate.AddDate(0, 1, -1)) {
			quantityReservationsThisMont++
		}
	}
	return quantityReservationsThisMont, nil
}
