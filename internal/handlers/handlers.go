package handlers

import (
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/financial"
	"restaurante/internal/repository/pagerepo"
	"restaurante/internal/repository/reception"
	"restaurante/internal/repository/supplier"
	"restaurante/internal/repository/users"
)

// PageRepo the repository used by the handlers
var PageRepo *PageRepository

// PageRepository is the repository type
type PageRepository struct {
	App         *config.Application
	PageDB      repository.PageRepository
	UserDB      repository.UserRepository
	SuplierDB   repository.SupplierRepository
	FinancialDB repository.FinancialRepository
	ReceptionDB repository.ReceptionRepository
}

func NewPageRepo(a *config.Application, db *driver.DB) *PageRepository {
	return &PageRepository{
		App:         a,
		PageDB:      pagerepo.NewPostgresRepo(db.SQL, a),
		UserDB:      users.NewPostgresRepo(db.SQL, a),
		SuplierDB:   supplier.NewPostgresRepo(db.SQL, a),
		FinancialDB: financial.NewPostgresRepo(db.SQL, a),
		ReceptionDB: reception.NewPostgresRepo(db.SQL, a),
	}
}

// NewPageHandlers sets the repository for the handlers
func NewPageHandlers(r *PageRepository) {
	PageRepo = r
}

// NotFoundPage is the handler for 404 page not found
func (m *PageRepository) NotFoundPage(w http.ResponseWriter, r *http.Request) {

	render.Template(w, r, "404.page.tmpl", &models.TemplateData{})
}
