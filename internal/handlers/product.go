package handlers

import (
	"errors"
	"net/http"
	"os"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/category"
	"restaurante/internal/repository/product"
	"restaurante/internal/repository/supplier"
	"time"

	"strconv"

	"github.com/go-chi/chi"
)

var ProductRepo *ProductRepository

type ProductRepository struct {
	App        *config.Application
	ProductDB  repository.ProductRepository
	CategoryDB repository.CategoryRepository
	SupplierDB repository.SupplierRepository
}

// NewProductRepo sets the repository for the handlers
func NewProductRepo(a *config.Application, db *driver.DB) *ProductRepository {
	return &ProductRepository{
		App:        a,
		ProductDB:  product.NewPostgresRepo(db.SQL, a),
		CategoryDB: category.NewPostgresRepo(db.SQL, a),
		SupplierDB: supplier.NewPostgresRepo(db.SQL, a),
	}
}

// NewProductHandlers sets the repository for the handlers
func NewProductHandlers(r *ProductRepository) {
	ProductRepo = r
}

// ProductPage displays the products page
func (m *ProductRepository) ProductsPage(w http.ResponseWriter, r *http.Request) {
	products, err := m.ProductDB.AllProducts()
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting products: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar produtos")
		return
	}

	categories, err := m.CategoryDB.GetAllCategories()
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting categories: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar categorias")
		return
	}

	data := make(map[string]interface{})
	data["products"] = products
	data["categories"] = categories

	render.Template(w, r, "product.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// OneProductPage displays the product page
func (m *ProductRepository) OneProductPage(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	prodID, _ := strconv.Atoi(id)

	product, err := m.ProductDB.GetProductByID(prodID)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting product: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar produto")
		return
	}

	categories, err := m.CategoryDB.GetAllCategories()
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting categories: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar categorias")
		return
	}

	suppliers, err := m.SupplierDB.GetAll()
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting suppliers: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar fornecedores")
		return
	}

	data := make(map[string]interface{})
	data["product"] = product
	data["categories"] = categories
	data["suppliers"] = suppliers

	render.Template(w, r, "product-edit.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateProduct creates a new product
func (m *ProductRepository) CreateProduct(w http.ResponseWriter, r *http.Request) {
	var inputProduct models.Product

	err := helpers.ReadJSON(w, r, &inputProduct)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: reading json data: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	msg := inputProduct.ValidateFields(inputProduct)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}
	inputProduct.SupplierID = 0
	inputProduct.Stock = 0
	inputProduct.PurchasePrice = 0

	err = m.ProductDB.CreateProduct(&inputProduct)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: creating product: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Produto criado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdateProduct updates a product
func (m *ProductRepository) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	prodID, _ := strconv.Atoi(id)

	var inputProduct models.Product

	err := helpers.ReadJSON(w, r, &inputProduct)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: reading json data: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	msg := inputProduct.ValidateFields(inputProduct)
	if msg != "" {
		helpers.BadRequest(w, r, errors.New(msg))
		return
	}

	productDB, err := m.ProductDB.GetProductByID(prodID)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting product: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	if productDB.SupplierID == 0 {
		inputProduct.SupplierID = 0
	}

	inputProduct.ID = prodID

	err = m.ProductDB.UpdateProduct(&inputProduct)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: updating product: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Produto atualizado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteProduct deletes a product
func (m *ProductRepository) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	prodID, _ := strconv.Atoi(id)

	err := m.ProductDB.DeleteProduct(prodID)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: deleting product: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Produto deletado com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// CreatePurchase creates a new purchase
func (m *ProductRepository) CreatePurchase(w http.ResponseWriter, r *http.Request) {
	userID := m.App.Session.GetInt(r.Context(), "user_id")

	var inputPurchase struct {
		ProductID  int     `json:"product_id"`
		SupplierID int     `json:"supplier_id"`
		Qtd        int     `json:"qtd"`
		SaleValue  float64 `json:"sale_value"`
	}

	err := helpers.ReadJSON(w, r, &inputPurchase)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: reading json data: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	if inputPurchase.SupplierID == 0 {
		helpers.BadRequest(w, r, errors.New("fornecedor inválido"))
		return
	}
	if inputPurchase.SaleValue == 0 || inputPurchase.SaleValue < 0 {
		helpers.BadRequest(w, r, errors.New("valor da venda inválido"))
		return
	}
	if inputPurchase.Qtd == 0 || inputPurchase.Qtd < 0 {
		helpers.BadRequest(w, r, errors.New("quantidade inválida"))
		return
	}

	totalSale := inputPurchase.SaleValue * float64(inputPurchase.Qtd)

	oldStock, err := m.ProductDB.GetStockByProductID(inputPurchase.ProductID)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting product stock: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}
	inputPurchase.Qtd += oldStock

	product := models.Product{
		ID:            inputPurchase.ProductID,
		SupplierID:    inputPurchase.SupplierID,
		Stock:         inputPurchase.Qtd,
		PurchasePrice: inputPurchase.SaleValue,
	}
	err = m.ProductDB.UpdateProductByPurchase(&product)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: updating product: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	purchase := models.PurchaseMoviment{
		Amount:       totalSale,
		PurchaseDate: time.Now(),
		UserID:       userID,
		SupplierID:   inputPurchase.SupplierID,
		Status:       "pago",
		Type:         "saida",
	}

	purchaseID, err := m.ProductDB.CreatePurchaseMoviments(&purchase)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: creating purchase moviment: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	billPayable := models.BillPayable{
		Amount:             totalSale,
		Description:        "Compra de produtos",
		UserID:             userID,
		PurchaseMovimentID: purchaseID,
		Status:             "pago",
		DueDate:            time.Now(),
		BillDate:           time.Now(),
	}

	err = m.ProductDB.CreateBillPayable(&billPayable)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: creating bill payable: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Compra realizada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// PurchasesPage lists all purchases
func (m *ProductRepository) PurchasesPage(w http.ResponseWriter, r *http.Request) {
	purchases, err := m.ProductDB.GetAllPurchases()
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting purchases: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar compras")
		return
	}

	data := make(map[string]interface{})
	data["purchases"] = purchases

	render.Template(w, r, "purchase.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// DeletePurchase deletes a purchase
func (m *ProductRepository) DeletePurchase(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	purchaseID, err := strconv.Atoi(id)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: converting purchase id: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	err = m.ProductDB.DeletePurchase(purchaseID)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: deleting purchase: %s\n", err)
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Compra deletada com sucesso!"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// LowStockPage displays the low stock page
func (m *ProductRepository) LowStockPage(w http.ResponseWriter, r *http.Request) {
	minStock := os.Getenv("STOCK_MIN")
	min, err := strconv.Atoi(minStock)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting min stock: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar estoque mínimo")
		return
	}

	products, err := m.ProductDB.GetLowStockProducts(min)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting low stock products: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar produtos com estoque baixo")
		return
	}

	suppliers, err := m.SupplierDB.GetAll()
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting suppliers: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar fornecedores")
		return
	}

	data := make(map[string]interface{})
	data["products"] = products
	data["suppliers"] = suppliers

	render.Template(w, r, "low-stock.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// ReportsProductsPage displays the reports products page
func (m *ProductRepository) ReportsProductsPage(w http.ResponseWriter, r *http.Request) {
	products, err := m.ProductDB.AllProducts()
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting products: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de produtos")
		http.Redirect(w, r, "/sistema/products", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["products"] = products

	render.Template(w, r, "products-reports.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// ReportsPurchasesPage displays the reports purchases page
func (m *ProductRepository) ReportsPurchasesPage(w http.ResponseWriter, r *http.Request) {
	// recuperar os parametro que vem na query string
	startDate := r.URL.Query().Get("purchase_start_date")
	endDate := r.URL.Query().Get("purchase_end_date")
	status := r.URL.Query().Get("purchase_report_status")

	if status == "0" {
		status = ""
	} else if status == "1" {
		status = "pago"
	} else if status == "2" {
		status = "pendente"
	}

	purchases, err := m.ProductDB.GetAllPurchasesFromReport(startDate, endDate, status)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: getting purchases: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de compras")
		http.Redirect(w, r, "/sistema/purchases", http.StatusSeeOther)
	}

	total := 0.0
	for _, purchase := range purchases {
		total += purchase.Amount
	}

	sDate, err := time.Parse("2006-01-02", startDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de compras")
		http.Redirect(w, r, "/sistema/purchases", http.StatusSeeOther)
	}
	eDate, err := time.Parse("2006-01-02", endDate)
	if err != nil {
		m.App.ErrorLog.Printf("ERROR: parsing start date: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao exibir relatórios de compras")
		http.Redirect(w, r, "/sistema/purchases", http.StatusSeeOther)
	}

	data := make(map[string]interface{})
	data["purchases"] = purchases
	data["start_date"] = sDate
	data["end_date"] = eDate
	data["total"] = total

	render.Template(w, r, "purchases-reports.page.tmpl", &models.TemplateData{
		Data: data,
	})
}
