package handlers

import (
	"errors"
	"log"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/category"
	"strconv"

	"github.com/go-chi/chi"
)

var CategoryRepo *CategoryRepository

// CategoryRepository is the repository type
type CategoryRepository struct {
	App          *config.Application
	CategoryRepo repository.CategoryRepository
}

// NewCategoryRepo sets the repository for the handlers
func NewCategoryRepo(a *config.Application, db *driver.DB) *CategoryRepository {
	return &CategoryRepository{
		App:          a,
		CategoryRepo: category.NewPostgresRepo(db.SQL, a),
	}
}

// NewCategoryHandlers sets the repository for the handlers
func NewCategoryHandlers(r *CategoryRepository) {
	CategoryRepo = r
}

// CategoriesPage is the handler used to render the categories page
func (m *CategoryRepository) CategoriesPage(w http.ResponseWriter, r *http.Request) {
	c, err := m.CategoryRepo.GetAllCategories()
	if err != nil {
		log.Printf("Error getting categories: %s\n", err)
		m.App.Session.Put(r.Context(), "error", "Erro ao buscar categorias")
		return
	}

	data := make(map[string]interface{})
	data["categories"] = c

	render.Template(w, r, "categories.page.tmpl", &models.TemplateData{
		Data: data,
	})
}

// CreateCategory is the handler used to create a new category
func (m *CategoryRepository) CreateCategory(w http.ResponseWriter, r *http.Request) {
	var c models.Category

	err := helpers.ReadJSON(w, r, &c)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	if c.Name == "" {
		helpers.BadRequest(w, r, errors.New("campo nome é obrigatório"))
		return
	}

	err = m.CategoryRepo.CreateCategory(&c)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Categoria criada com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// UpdateCategory is the handler used to update a category
func (m *CategoryRepository) UpdateCategory(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	categoryID, _ := strconv.Atoi(id)

	var c models.Category

	err := helpers.ReadJSON(w, r, &c)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	if c.Name == "" {
		helpers.BadRequest(w, r, errors.New("campo nome é obrigatório"))
		return
	}

	c.ID = categoryID

	err = m.CategoryRepo.UpdateCategory(&c)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Categoria atualizada com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// DeleteCategory is the handler used to delete a category
func (m *CategoryRepository) DeleteCategory(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	categoryID, _ := strconv.Atoi(id)

	err := m.CategoryRepo.DeleteCategory(categoryID)
	if err != nil {
		helpers.ServerError(w, r, errors.New("internal server error"))
		return
	}

	var payload struct {
		Error   bool   `json:"error"`
		Message string `json:"message"`
	}
	payload.Error = false
	payload.Message = "Categoria deletada com sucesso"

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}
