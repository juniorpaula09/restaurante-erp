package handlers

import (
	"errors"
	"net/http"
	"restaurante/internal/config"
	"restaurante/internal/driver"
	"restaurante/internal/helpers"
	"restaurante/internal/models"
	"restaurante/internal/render"
	"restaurante/internal/repository"
	"restaurante/internal/repository/auth"
	"restaurante/internal/repository/users"
	"restaurante/internal/validation"
	"time"
)

// Repo the repository used by the handlers
var AuthRepo *AuthRepository

// Repository is the repository type
type AuthRepository struct {
	App    *config.Application
	AuthDB repository.AuthRepository
	UserDB repository.UserRepository
}

func NewAuthRepo(a *config.Application, db *driver.DB) *AuthRepository {
	return &AuthRepository{
		App:    a,
		AuthDB: auth.NewPostgresRepo(db.SQL, a),
		UserDB: users.NewPostgresRepo(db.SQL, a),
	}
}

// NewHandlers sets the repository for the handlers
func NewAuthHandlers(r *AuthRepository) {
	AuthRepo = r
}

// LoginPage handles the login page GET request
func (m *AuthRepository) LoginPage(w http.ResponseWriter, r *http.Request) {
	render.Template(w, r, "login.page.tmpl", &models.TemplateData{
		Form: validation.New(nil),
	})
}

// CreateAuthToken handles the authentication POST request
func (m *AuthRepository) CreateAuthToken(w http.ResponseWriter, r *http.Request) {
	m.App.Session.RenewToken(r.Context())

	var userInput struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	err := helpers.ReadJSON(w, r, &userInput)
	if err != nil {
		helpers.ServerError(w, r, err)
		return
	}

	// get the user from database based on email address; send error if not found
	user, err := m.UserDB.GetUserByEmail(userInput.Email)
	if err != nil {
		helpers.NotFound(w, r, errors.New("email inválido"))
		return
	}

	// validate password; send error if not valid
	validPassword, err := helpers.PasswordMatch(user.Password, userInput.Password)
	if !validPassword || err != nil {
		helpers.Unauthorized(w, errors.New("credenciais inválidas"))
		return
	}

	// create a new token
	token, err := m.AuthDB.GenerateToken(user.ID, 24*time.Hour, models.ScopeAuthentication)
	if err != nil {
		helpers.BadRequest(w, r, err)
		return
	}

	// save to the database
	err = m.AuthDB.InsertToken(token, user)
	if err != nil {
		helpers.BadRequest(w, r, errors.New("error ao criar a sessão"))
		return
	}

	var payload struct {
		Error       bool          `json:"error"`
		Message     string        `json:"message"`
		Token       *models.Token `json:"access_token"`
		AccessLevel int           `json:"access_level"`
	}
	payload.Error = false
	payload.Message = "Token criado com sucesso"
	payload.Token = token
	payload.AccessLevel = user.AccessLevel

	// put in the session
	m.App.Session.Put(r.Context(), "user_id", user.ID)
	m.App.Session.Put(r.Context(), "access_level", user.AccessLevel)

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// IsAuthenticated handles the authentication POST request
func (m *AuthRepository) IsAuthenticated(w http.ResponseWriter, r *http.Request) {
	token, err := helpers.AuthenticateToken(r)
	if err != nil {
		helpers.Unauthorized(w, err)
		return
	}

	user, err := m.UserDB.GetUserByToken(token)
	if err != nil {
		helpers.NotFound(w, r, errors.New("usuário não encontrado"))
		return
	}

	var payload struct {
		Error   bool        `json:"error"`
		Message string      `json:"message"`
		User    models.User `json:"user"`
	}
	payload.Error = false
	payload.Message = "Usuário autenticado com sucesso"
	payload.User = models.User{
		ID:          user.ID,
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Email:       user.Email,
		AccessLevel: user.AccessLevel,
	}

	_ = helpers.WriteJSON(w, http.StatusOK, payload)
}

// Logout handles the logout GET request
func (m *AuthRepository) Logout(w http.ResponseWriter, r *http.Request) {
	_ = m.App.Session.Destroy(r.Context())
	_ = m.App.Session.RenewToken(r.Context())

	http.Redirect(w, r, "/", http.StatusSeeOther)
}
