let editForm = document.getElementById('btn-edit-form');
editForm.addEventListener('click', editUser);

function editUser(evt) {
    evt.preventDefault();

    let name = document.getElementById('name').value;
    let lastname = document.getElementById('lastname').value;
    let email = document.getElementById('email').value;
    let csrf_token = document.getElementById('csrf_token').value;

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'X-CSRF-Token': csrf_token },
        body: JSON.stringify({ name: name, lastname: lastname, email: email })
    }


    fetch('/sistema/edit-user', requestOptions)
        .then(response => response.json())
        .then(data => {
            if (data.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: data.message,
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#dc3545'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                })
            } else {
                Swal.fire({
                    icon: 'success',
                    title: 'Usuario atualizado',
                    text: data.message,
                    confirmButtonText: 'Ok',
                    confirmButtonColor: '#28a745'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
                })
            }
        })
        .catch(error => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: "Ocorreu um erro ao atualizar o usuário!"
            })
        });
}