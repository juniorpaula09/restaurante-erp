INSERT INTO users (id, first_name, last_name, email, password, access_level, created_at, updated_at)
VALUES (1, 'admin', 'system' , 'admin@local.com.br', '$2a$12$8SpSl0jDR5/mVEbCYtlXqeuncFAL1EkCDU8ZLu5P6pDYl4/2WT73y', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
