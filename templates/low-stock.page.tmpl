{{template "base" .}}

{{define "css"}}
<style>
    .dataTables_wrapper {
        margin: 0 auto;
    }

    .stock-header {
        display: flex;
        justify-content: space-between;
        margin: 0 auto;
        margin-bottom: 40px;
    }
</style>
{{end}}

{{define "title"}}
Estoque baixo
{{end}}

{{define "content"}}
<div class="container-fluid p-4">
    <div class="row">
        <div class="col-md-12 stock-header">
            <h3>Estoque Baixo</h3>
        </div>

        <div class="col-md-12">
            {{$products := index .Data "products"}}
            <table class="table table-hover table-striped" id="stock-table">
                <thead class="table-primary">
                    <tr>
                        <th>COD</th>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th>Valor compra</th>
                        <th>Valor venda</th>
                        <th>Qtd Estoque</th>
                        <th>Categoria</th>
                        <th>Fornecedor</th>
                        <th>Data de cadastro</th>
                        <th class="d-flex justify-content-end">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {{range $products}}
                    <tr>
                        <td><strong>#{{.ID}}</strong></td>
                        <td>{{.Name}}</td>
                        <td>{{.Description}}</td>
                        <td>R$ {{formatCurrency .PurchasePrice}}</td>
                        <td>R$ {{formatCurrency .SalePrice}}</td>
                        <td>{{.Stock}}</td>
                        <td>{{.Category.Name}}</td>
                        <td>{{.Supplier.Name}}</td>
                        <td>{{formatDate .CreatedAt}}</td>
                        <td class="d-flex justify-content-end">
                            <button class="btn btn-sm btn-success pointer badge" onclick="setAttId('{{.ID}}')" data-bs-toggle="modal" data-bs-target="#purchases">
                                Comprar
                            </button>
                        </td>
                    </tr>
                    {{end}}
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- modal purchases -->
<div class="modal fade" id="purchases" tabindex="-1" aria-labelledby="purchasesModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="purchasesModalLabel">Adicionar às compras</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form id="purchases-form">
            <div class="modal-body">
                <input type="hidden" id="csrf_token" name="csrf_token" value="{{.CSRFToken}}">
                <input type="hidden" id="product_id" name="product_id" value="">

                <div class="row mt-3">
                    <div class="col-md-12">
                        {{$supplier := index .Data "suppliers"}}
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="addon-purchase-supplier">Fornecedor</span>
                            <select class="form-select" id="low_supplier" aria-label="addon-purchase-supplier">
                                <option selected>Selecione o Fornecedor</option>
                                {{range $supplier}}
                                <option value="{{.ID}}">{{.Name}}</option>
                                {{end}}
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="addon-purchase-qtd">Quantidade</span>
                            <input type="number" class="form-control" name="purchase_qtd"
                                id="low_qtd" aria-label="purchaseQtd"
                                aria-describedby="addon-purchase-qtd">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="addon-purchase-sale-value">Valor da
                                compra</span>
                            <input type="text" class="form-control" name="purchase_sale_value"
                                id="low_sale_value" aria-label="purchaseSaleValue"
                                aria-describedby="addon-purchase-sale-value">
                            <span class="input-group-text">R$</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="reloadPage()" data-bs-dismiss="modal">Fechar</button>
            <button type="button" id="btn-low"  class="btn btn-primary">Salvar</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- end modal purchases-->

{{end}}

{{define "js"}}
<script>
    new DataTable('#stock-table', {
        order: [],
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.24/i18n/Portuguese-Brasil.json'
        }
    });

    setAttId = (id) => {
        document.getElementById("product_id").value = id;
    }

    let btnPurchase = document.querySelector('#btn-low');
    btnPurchase.addEventListener('click', createPurchase);

    function createPurchase(evt) {
        evt.preventDefault();

        const payload = {
            product_id: parseInt(document.getElementById("product_id").value),
            supplier_id: parseInt(document.getElementById("low_supplier").value),
            qtd: parseFloat(document.getElementById("low_qtd").value),
            sale_value: parseFloat(document.getElementById("low_sale_value").value),
        }

        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.getElementById("csrf_token").value
            },
            body: JSON.stringify(payload)
        };

        fetch('/sistema/create-purchase', requestOptions)
            .then(response => response.json())
            .then(data => {
                if (data.error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: data.message,
                    })
                } else {
                    Swal.fire({
                        icon: 'success',
                        title: 'Sucesso!',
                        text: data.message,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            let form = document.getElementById("purchases-form");
                            form.reset();
                        }
                    })
                }
            })
            .catch((error) => {
                console.error('Error:', error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Ocorreu um erro ao tentar criar a compra!',
                })
            });
    }

    let shortDescription = document.querySelectorAll('td:nth-child(3)')
    shortDescription.forEach((description) => {
        let text = description.innerText;
        if (text.length > 30) {
            description.innerText = text.substring(0, 30) + '...';
        }
    })

    function reloadPage() {
        window.location.reload();
    }

</script>
{{end}}