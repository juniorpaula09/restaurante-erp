{{define "base"}}
<!doctype html>
<html lang="pt-BR">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.5/dist/sweetalert2.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- CDN Data tables -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" />
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    
    <!-- fontes -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,300;0,400;0,500;0,600;0,700;1,100;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/static/img/icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/static/img/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/static/img/icon/favicon-16x16.png">
    <link rel="manifest" href="/static/img/icon/site.webmanifest">

    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            list-style: none;
        }
        
        body {
            font-family: 'Work Sans', sans-serif;
            background-color: #fdfdfd;
            overflow-x: hidden;
        }

        .btn.btn-primary {
            background-color: #4585e2;
            color: #fff;
            font-weight: 500;
            font-size: 16px;
        }

        .btn.btn-success {
            background-color: #4aba58;
            color: #fff;
            font-weight: 500;
            font-size: 16px;
        }

        .btn.btn-secondary {
            background-color: #c1c1c1;
            color: #333333;
            font-weight: 400;
            font-size: 16px;
            border-color: transparent;
        }

        .btn.btn-warning {
            background-color: #f7b924;
            color: #333333;
            font-weight: 400;
            font-size: 16px;
            border-color: transparent;
        }

        h1,h2,h3,h4,h5,h6 {
            color: #333333;
        }

        .modal-header {
            background-color: #F1F4F9;
            color: #333333;
        }

        .my-footer {
            background-color: #eceaea;
            color: #333333;
            bottom: 0;
            left: 0;
            height: 65px;
            position: fixed;
            width: 100%;
            padding: 10px 0;
        }

        .errors-message {
            margin-top: 10px;
            text-align: center;
        }
        .navbar {
            background-color: #F1F4F9;
            color: #c1c1c1;
            padding: 15px 0;
        }
        .nav-link {
            font-weight: 500;
            font-size: 16px;
            color: #333333;
        }
        .navbar-brand {
            font-weight: 600;
            font-size: 18px;
            color: #4585e2;
        }
        .navbar-brand:hover {
            color: #4585e2;
        }

        th {
            color: #333333 !important;
            font-weight: 600 !important;
        }

    </style>

    {{block "css" .}}

    {{end}}

    <title>{{block "title" .}} {{end}}</title>

</head>

<body>
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <a class="navbar-brand" href="/sistema/home">ERP Restaurante</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    {{if or (eq .User.AccessLevel 1) (eq .User.AccessLevel 2)}}
                    <li class="nav-item active">
                        <a class="nav-link active" aria-current="page" href="/sistema/home">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Pessoas
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            {{if eq .User.AccessLevel 1}}
                            <li><a class="dropdown-item" href="/sistema/users">Usuários</a></li>
                            {{end}}
                            <li><a class="dropdown-item" href="/sistema/employees">Funcionários</a></li>
                            <li><a class="dropdown-item" href="/sistema/suppliers">Fornecedores</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Cadastros
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            {{if eq .User.AccessLevel 1}}
                            <li><a class="dropdown-item" href="/sistema/positions">Cargos</a></li>
                            {{end}}
                            <li><a class="dropdown-item" href="/sistema/tables">Mesas</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Produtos/Pratos
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="/sistema/products">Produtos</a></li>
                            <li><a class="dropdown-item" href="/sistema/categories">Categorias</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="/sistema/dishes">Pratos</a></li>
                            <li><a class="dropdown-item" href="/sistema/low-stock">Estoque baixo</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Relatórios
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="/sistema/reports-products" target="_blank">Produtos</a></li>
                            <li><a class="dropdown-item" href="/sistema/reports-dishes" target="_blank">Pratos</a></li>
                            <li><a class="dropdown-item" href="/sistema/purchases">Compras</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="/sistema/moviments">Movimentações</a></li>
                            <li><a class="dropdown-item" href="/sistema/billpay">Contas à Pagar</a></li>
                            <li><a class="dropdown-item" href="/sistema/bill-receive">Contas à Receber</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Movimentações
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li>
                                <a class="dropdown-item" aria-current="page" href="/sistema/purchases">Compras</a>
                            </li>
                            <li>
                                <a class="dropdown-item" aria-current="page" href="/sistema/moviments">Movimentações</a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <a class="dropdown-item" aria-current="page" href="/sistema/billpay">Contas à Pagar</a>
                            </li>
                            <li>
                                <a class="dropdown-item" aria-current="page" href="/sistema/bill-receive">Contas à Receber</a>
                            </li>
                        </ul>
                    </li>
                    {{end}}
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/sistema/clients">Clientes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="/sistema/reservations">Reservas</a>
                    </li>
                </ul>

               <div style="margin-right: 20px;">
                    <form class="d-flex navbar-nav mr-5">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <span id="user-logged"></span>
                            </a>
                            <ul class="dropdown-menu mr-5" aria-labelledby="navbarDropdown">
                                <li>
                                    <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#edit-profile" href="!#">Editar Perfil</a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li>
                                    <a class="dropdown-item" onclick="logout()" href="/logout">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </form>
               </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="errors-message">
                    {{with .Flash}}
                    <div class="alert alert-success" role="alert">
                        {{.}}
                    </div>
                    {{end}}
                    {{with .Error}}
                    <div class="alert alert-danger" role="alert">
                        {{.}}
                    </div>
                    {{end}}    
                </div>
            </div>
        </div>
    </div>

    {{block "content" .}}

    {{end}}


    <footer class="my-footer">
        <div class="row">
            <div class="col text-center">
                <strong>EPR &amp; Restaurante</strong><br>
                <strong>Celular:</strong> (47) 99156-8787 <br>
            </div>

            <div class="col text-center">
                
            </div>

            <div class="col text-center">
                <strong>Sistemas<br>para Restaurantes</strong>
            </div>
        </div>
    </footer>

    <!-- modal edit profile -->
    <div class="modal fade" id="edit-profile" tabindex="-1" aria-labelledby="editProfileModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProfileModalLabel">Editar Perfil</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="edit-form">
                <div class="modal-body">
                    <input type="hidden" id="csrf_token" name="csrf_token" value="{{.CSRFToken}}">

                    <div class="row mt-3">
                    <div class="col-6">
                        <div class="input-group mb-3">
                        <span class="input-group-text" id="addon-name">Nome</span>
                        <input type="text" class="form-control" name="name" id="name" aria-label="Username" aria-describedby="addon-name" value="{{.User.FirstName}}">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group mb-3">
                        <span class="input-group-text" id="addon-lastname">Sobrenome</span>
                        <input type="text" class="form-control" name="lastname" id="lastname" aria-label="Lastname" aria-describedby="addon-lastname" value="{{.User.LastName}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-group mb-3">
                        <span class="input-group-text" id="addon-email">Email</span>
                        <input type="email" class="form-control" name="email" id="email" aria-label="Email" aria-describedby="addon-email" value="{{.User.Email}}">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Fechar</button>
                <button type="submit" id="btn-edit-form"  class="btn btn-sm btn-primary">Salvar</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.5/dist/sweetalert2.min.js"></script>
    <script src="/static/js/user.js"></script>
    
    <script>
        // Verifica se o usuário está logado
        checkAuth()

        function checkAuth() {
            if (localStorage.getItem('token') === null) {
                console.log('no token')
                window.location.href = '/'
            } else {
                let tokenExpiry = localStorage.getItem('token_expiry')
                let now = new Date().getTime()

                if (now > tokenExpiry) {
                    window.location.href = '/'
                }

                let token = localStorage.getItem('token')
                const headers = new Headers()
                headers.append('Content-Type', 'application/json')
                headers.append('X-CSRF-Token', '{{.CSRFToken}}')
                headers.append('Authorization', `Bearer ${token}`)

                const requestOptions = {
                    method: 'POST',
                    headers: headers,
                };

                fetch(`/is-authenticated`, requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        if (data.error) {
                            localStorage.removeItem('token')
                            localStorage.removeItem('token_expiry')
                            localStorage.removeItem('__user')
                            window.location.href = '/'
                        } else {
                            console.log(data.user)
                            localStorage.setItem('__user', JSON.stringify(data.user))
                            let username = document.getElementById('user-logged')
                            username.innerHTML = data.user.first_name + ' ' + data.user.last_name 
                        }
                    }).catch(error => {
                        localStorage.removeItem('token')
                        localStorage.removeItem('token_expiry')
                        localStorage.removeItem('__user')
                        window.location.href = '/'
                    });

            }
        }
       
        function logout() {
            localStorage.removeItem('token');
            localStorage.removeItem('token_expiry');
            localStorage.removeItem('user');
            window.location.href = '/';
        }
    </script>

    {{block "js" .}}

    {{end}}

</body>

</html>
{{end}}